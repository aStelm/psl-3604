#ifndef MAINH
#define MAINH

#ifdef PSL_Exports
#define PSL_API __declspec(dllexport)
#else
#define PSL_API __declspec(dllimport)
#endif

extern "C"
 {
//������� ���� bool ���������� true,
//���� ������� ���������� �����.

//��������� ���������� � ������ "PSL-3604".
  PSL_API bool WINAPI PSL_OpenDevice(void);

//��������� ����������.
  PSL_API bool WINAPI PSL_CloseDevice(void);

//���������� handle ����������.
  PSL_API HANDLE WINAPI PSL_GetDeviceHandle(void);

//���������� ��������� �� ������ � ������ ��������� ������
//(��� ������ ������, ���� ��� ������).
  PSL_API void WINAPI PSL_GetLastError(LPCSTR &lpcStr);

//���������� ��������� �� ������ � ������ ����������.
  PSL_API bool WINAPI PSL_GetInfo(LPCSTR &lpcStr);

//������ ����������, ��� � ��������� ������:
//v = 0..VMAX, x0.01 �,
//i = 0..IMAX, x0.001 �,
//on = 0 - ����� ��������, on = 1 - ����� �������.
  PSL_API bool WINAPI PSL_SetVI(int v, int i, bool on);

//���������� �������� ����������, ��� � ��������� ������:
//v = 0..VMAX, x0.01 �,
//i = 0..IMAX, x0.001 �,
  PSL_API bool WINAPI PSL_GetVI(int &v, int &i);

//������ ��������� ���������:
//s.0 = 1 - ����� �������.
//s.1 = 1 - ����� CV.
//s.2 = 1 - ����� CC.
//s.3 = 1 - ��������� OVP.
//s.4 = 1 - ��������� OCP.
//s.5 = 1 - ��������� OPP.
//s.6 = 1 - ��������� OTP.
  PSL_API bool WINAPI PSL_GetStat(int &s);

//���������� ������� ���������� ���������� � ���:
//v = 0..VMAX, x0.01 �,
//i = 0..IMAX, x0.001 �.
  PSL_API bool WINAPI PSL_GetVIavg(int &v, int &i);

//���������� ���������� ���������� ���������� � ���:
//v = 0..VMAX, x0.01 �,
//i = 0..IMAX, x0.001 �.
  PSL_API bool WINAPI PSL_GetVIfast(int &v, int &i);

//������ ������������ ����������, ��� � ��������:
//v = 1000..9999, x0.01 �,
//i = 1000..9999, x0.001 �,
//p = 10..9999, x0.1 ��.
  PSL_API bool WINAPI PSL_SetVIPmax(int v, int i, int p);

//���������� ������������ ����������, ��� � ��������:
//v = 1000..9999, x0.01 �,
//i = 1000..9999, x0.001 �,
//p = 10..9999, x0.1 ��.
  PSL_API bool WINAPI PSL_GetVIPmax(int &v, int &i, int &p);

//������ ������ ������:
//v = 0..VMAX, x0.01 �,
//i = 0..IMAX, x0.001 �,
//p = 0..PMAX, x0.1 ��.
  PSL_API bool WINAPI PSL_SetProt(int v, int i, int p);

//���������� ������ ������:
//v = 0..VMAX, x0.01 �,
//i = 0..IMAX, x0.001 �,
//p = 0..PMAX, x0.1 ��.
  PSL_API bool WINAPI PSL_GetProt(int &v, int &i, int &p);

//������ �������:
//p = 0..9 - ����� �������,
//v = 0..VMAX, x0.01 �,
//i = 0..IMAX, x0.001 �.
  PSL_API bool WINAPI PSL_SetPre(int p, int v, int i);

//������ �������:
//p = 0..9 - ����� �������,
//v = 0..VMAX, x0.01 �,
//i = 0..IMAX, x0.001 �.
  PSL_API bool WINAPI PSL_GetPre(int p, int &v, int &i);

//��������� ���������:
//p - ����� ���������,
//v - �������� ���������.
  PSL_API bool WINAPI PSL_SetPar(int p, int v);

//������ ���������:
//p - ����� ���������,
//v - �������� ���������.
  PSL_API bool WINAPI PSL_GetPar(int p, int &v);

//������ �������� ����������� � �����������:
//s = 0..100 - �������� �����������, %,
//t = 0..999 - �����������, x0.1�C.
  PSL_API bool WINAPI PSL_GetFan(int &s, int &t);

//��������� ���� ���:
//cv = 0..65520 - ��� ��� ����������,
//ci = 0..65520 - ��� ��� ����.
  PSL_API bool WINAPI PSL_SetDAC(int cv, int ci);

//������ ���� ���:
//cv = 0..65520 - ��� ��� ����������,
//ci = 0..65520 - ��� ��� ����.
  PSL_API bool WINAPI PSL_GetADC(int &cv, int &ci);

//��������� �������������� ������������:
//� - ����� ������������,
//v - �������� ������������.
  PSL_API bool WINAPI PSL_SetCal(int c, int v);

//������ �������������� ������������:
//� - ����� ������������,
//v - �������� ������������.
  PSL_API bool WINAPI PSL_GetCal(int c, int &v);
 }

#endif


