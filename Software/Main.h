#ifndef PhCnt_MainH
#define PhCnt_MainH

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ToolWin.hpp>

#include <vcl\Registry.hpp>
#include <Buttons.hpp>
#include <ActnList.hpp>
#include <ImgList.hpp>
#include <Dialogs.hpp>
#include <Grids.hpp>

#include "ThemeMgr.hpp"
#include "FloatEdit.hpp"
#include "pslusb.h"

#define SOFTNAME "PowerView"
#define LOGN   18001 //������������ ���������� ����� �������
#define ROUND(x) ((x > 0)? (x + 0.5) : (x - 0.5))

//---------------------------------------------------------------------------

class TMainForm : public TForm
{
__published:	// IDE-managed Components
  TMainMenu *MainMenu;
  TMenuItem *mnFile;
  TMenuItem *mnNew;
  TMenuItem *mnOpen;
  TMenuItem *mnSaveAs;
  TMenuItem *mnSep1;
  TMenuItem *mnExit;
  TMenuItem *mnDevice;
  TMenuItem *mnStart;
  TMenuItem *mnPause;
  TMenuItem *mnStop;
  TMenuItem *mnView;
  TMenuItem *mnCursor;
  TMenuItem *mnSep3;
  TMenuItem *mnColours;
  TMenuItem *mnHelp;
  TMenuItem *mnAbout;
	TActionList *ActionList1;
	TAction *acNew;
  TAction *acOpen;
	TAction *acSaveas;
  TAction *acExit;
	TAction *acStart;
  TAction *acPause;
	TAction *acStop;
	TAction *acCursor;
  TAction *acColours;
  TAction *acAbout;
  TToolBar *ToolBar;
	TToolButton *tbNew;
  TToolButton *tbOpen;
	TToolButton *tbSaveAs;
  TToolButton *tbSep1;
	TToolButton *tbStart;
  TToolButton *tbPause;
	TToolButton *tbStop;
  TToolButton *tbSep2;
	TToolButton *tbCursor;
  TToolButton *tbSep4;
  TToolButton *tbColours;
	TImageList *ImageList1;
  TOpenDialog *OpenDialog;
	TSaveDialog *SaveDialog;
	TTimer *Timer1;
  TStatusBar *StatusBar;
  TAction *acSettings;
  TToolButton *ToolButton2;
  TToolButton *ToolButton3;
  TMenuItem *N1;
  TMenuItem *Settings1;
  TPanel *CtrlPanel;
  TPanel *UnderLogPanel;
  TScrollBar *LogScrollBar;
  TPanel *TlPanel;
  TAction *acConnect;
  TAction *acDisconnect;
  TMenuItem *Connect1;
  TMenuItem *Disconnect1;
  TMenuItem *N2;
  TToolButton *ToolButton1;
  TToolButton *ToolButton4;
  TToolButton *ToolButton5;
  TPanel *TrsPanel;
  TLabel *lbTraces;
  TCheckBox *cbVgr;
  TLabel *lbVgr;
  TCheckBox *cbIgr;
  TLabel *lbIgr;
  TCheckBox *cbPgr;
  TLabel *lbPgr;
  TCheckBox *cbTgr;
  TLabel *lbTgr;
  TCheckBox *cbFgr;
  TLabel *lbFgr;
  TAction *acRead;
  TAction *acWrite;
  TToolButton *ToolButton6;
  TToolButton *ToolButton7;
  TMenuItem *ReadParams1;
  TMenuItem *WriteParams1;
  TMenuItem *N3;
  TToolButton *ToolButton8;
  TThemeManager *ThemeManager1;
  TPanel *LogPanel;
  TPaintBox *LogPaintBox;
  TAction *acCutL;
  TAction *acCutR;
  TMenuItem *CutLeft1;
  TMenuItem *CutRight1;
  TMenuItem *N6;
  TAction *acFreeze;
  TToolButton *ToolButton9;
  TMenuItem *Freeze1;
  TGroupBox *GroupBox1;
  TEdit *edV;
  TLabel *Label4;
  TRadioButton *rbCV;
  TEdit *edI;
  TLabel *Label5;
  TRadioButton *rbCC;
  TGroupBox *GroupBox2;
  TScrollBar *sbV;
  TFloatEdit *feV;
  TLabel *Label1;
  TScrollBar *sbI;
  TFloatEdit *feI;
  TLabel *Label2;
  TRadioButton *rbOut;
  TButton *btOn;
  TButton *btOff;
  TEdit *Edit1;
  TEdit *Edit2;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall acNewExecute(TObject *Sender);
  void __fastcall acOpenExecute(TObject *Sender);
	void __fastcall acSaveasExecute(TObject *Sender);
  void __fastcall acExitExecute(TObject *Sender);
	void __fastcall acStartExecute(TObject *Sender);
  void __fastcall acPauseExecute(TObject *Sender);
	void __fastcall acStopExecute(TObject *Sender);
	void __fastcall acCursorExecute(TObject *Sender);
  void __fastcall acColoursExecute(TObject *Sender);
  void __fastcall acAboutExecute(TObject *Sender);
  void __fastcall acSettingsExecute(TObject *Sender);
  void __fastcall acConnectExecute(TObject *Sender);
  void __fastcall acDisconnectExecute(TObject *Sender);
  void __fastcall acReadExecute(TObject *Sender);
  void __fastcall acWriteExecute(TObject *Sender);
  void __fastcall acCutLExecute(TObject *Sender);
  void __fastcall acCutRExecute(TObject *Sender);
  void __fastcall acFreezeExecute(TObject *Sender);
  void __fastcall LogPaintBoxMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
  void __fastcall LogPaintBoxMouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
  void __fastcall LogPaintBoxPaint(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
  void __fastcall LogScrollBarChange(TObject *Sender);
  void __fastcall cbVgrClick(TObject *Sender);
  void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
  void __fastcall feVChanged(TObject *Sender);
  void __fastcall sbVChange(TObject *Sender);
  void __fastcall feIChanged(TObject *Sender);
  void __fastcall sbIChange(TObject *Sender);
  void __fastcall btOnClick(TObject *Sender);
  void __fastcall btOffClick(TObject *Sender);
  void __fastcall rbOutClick(TObject *Sender);
private:	// User declarations
  bool Lock;                  //���� ���������� ������������ ���������
  AnsiString FDataDir;        //���� ��� ���������� ������ ������
  AnsiString FFileName;       //��� ����� ������
  bool FModified;             //������� ��������� ������
  double DataV[LOGN];         //������ ������� ������ V
  double DataI[LOGN];         //������ ������� ������ I
  double DataP[LOGN];         //������ ������� ������ P
  double DataT[LOGN];         //������ ������� ������ T
  double DataF[LOGN];         //������ ������� ������ Fan
  int FPointer;               //��������� ������
  int FMode;                  //����� (stop, pause, start)
  enum { MD_STOP, MD_PAUSE, MD_START }; //���� ������ �������
  bool FCursor;               //����� ������� (on/off)
  bool FCurOn;                //������� ���������� �������
  int FCursorX;               //������� ���������� ������� X
  Graphics::TBitmap *Bm;
	void __fastcall WmDropFiles(TWMDropFiles& Message);
	BEGIN_MESSAGE_MAP
		MESSAGE_HANDLER(WM_DROPFILES, TWMDropFiles, WmDropFiles)
	END_MESSAGE_MAP(TForm);
  void __fastcall Connect(void);              //����������� ����������
  void __fastcall Disconnect(void);           //���������� ����������
	void __fastcall ReadConfig(void);           //������ ������������
	void __fastcall SaveConfig(void);           //���������� ������������
  void __fastcall Clear(void);                //������� ������
  void __fastcall ReadData(AnsiString Name);  //������ ������
  void __fastcall SaveData(AnsiString Name);  //���������� ������
  void __fastcall DrawLogGrid(TCanvas *cv);   //���������� �����
  void __fastcall DrawSegment(TCanvas *cv, TColor cl, double *data, //��������� 
                    double range, int shift, int x0, int x, int dy); //��������
  void __fastcall DrawLog(TCanvas *cv, int x1, int x2);   //���������� ��������
	void __fastcall DrawLogCur(TCanvas *cv, bool e, int x); //���������� �������

public:		// User declarations
	__fastcall TMainForm(TComponent* Owner);
  TColor Col_Bg, Col_Ax, Col_Tx, Col_Cr, Col_G1, Col_G2, Col_G3, Col_G4; //�����
  bool FConnected;            //���� ���������� � �����������
  bool FAutoConnect;          //���� ��������������� �����������
  bool FAutoLoad;             //���� �������������� �������� ����������
  int FSample;                //������ ������
  bool FAutoScroll;           //������� ��������������� ��������������
  void __fastcall SetDefColours(void); //��������� ������ �� ���������
  void __fastcall RedrawAll(void);     //����������� �����
};

//---------------------------------------------------------------------------

extern PACKAGE TMainForm *MainForm;

//---------------------------------------------------------------------------

#endif
