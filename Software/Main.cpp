#include <vcl.h>
#include <fstream> 
#include <iostream.h>
#include <math.h>
#include <dir.h>
#pragma hdrstop

#include "Main.h"
#include "About.h"
#include "Colours.h"
#include "Settings.h"

#pragma package(smart_init)
#pragma link "FloatEdit"
#pragma link "ThemeMgr"
#pragma resource "*.dfm"
TMainForm *MainForm;

//---------------------------------------------------------------------------

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
}

//-------------------------- �������� �����: --------------------------------

void __fastcall TMainForm::FormCreate(TObject *Sender)
{ int i;
  //����� ���������:
  Caption = Application->Title;      //��������� ����������
  DecimalSeparator = '.';            //���������� �����������
  FFileName = "";
  DragAcceptFiles(Handle, 1);        //���������� drag-n-drop
  //��������� DoubleBuffered ��� �����������:
  MainForm->DoubleBuffered = 1;
  UnderLogPanel->DoubleBuffered = 1;
  LogPanel->DoubleBuffered = 1;
  CtrlPanel->DoubleBuffered = 1;
  TrsPanel->DoubleBuffered = 1;
  ToolBar->DoubleBuffered = 1;
  TlPanel->DoubleBuffered = 1;
  StatusBar->DoubleBuffered = 1;
  //������������� ����������:
  Bm = new Graphics::TBitmap;
  FCursor = 0;
  FCursorX = 0;    //��������� ���������� �������
  SetDefColours(); //��������� ������ �� ���������
  FModified = 0;
  //������ ������������:
  Lock = 1;
  ReadConfig();
  Lock = 0;
  //������� �����:
  FMode = MD_STOP;                    //������ ������� �����������
  Disconnect();
  Timer1->Interval = FSample;
  Timer1->Enabled = 1;                //���������� �������
}

//----------------------- ������ �������� �����: ----------------------------

void __fastcall TMainForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
	int Ans = IDNO;
  if(FModified)
   Ans = MessageDlg("Data modified. Save changes?", mtConfirmation,
                    TMsgDlgButtons() << mbYes << mbNo << mbCancel, 0);
  if(Ans == IDYES) acSaveasExecute(NULL);
  if(Ans == IDCANCEL) CanClose = 0;
}

//-------------------------- �������� �����: --------------------------------

void __fastcall TMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
  SaveConfig();
  Disconnect();
  delete Bm;
}

//------------------ �������� ����� �� Drag-n-drop: -------------------------

void __fastcall TMainForm::WmDropFiles(TWMDropFiles &Message)
{
	HDROP drop_handle = (HDROP)Message.Drop;
	char fName[MAXPATH];
	DragQueryFile(drop_handle, 0, fName, MAXPATH);
	ReadData(fName);
	DragFinish(drop_handle);
}

//-------------------- ��������� ������ �� ���������: -----------------------

void __fastcall TMainForm::SetDefColours(void)
{
  Col_Bg = (TColor)16777215; //RGB(255,255,255);
  Col_Ax = (TColor)13948116; //RGB(172,172,172);
  Col_Tx = (TColor)4737096;  //RGB(0,0,0);
  Col_Cr = (TColor)0;        //RGB(0,0,0);
  Col_G1 = (TColor)32768;    //RGB(255,0,0);
  Col_G2 = (TColor)255;      //RGB(0,128,0);
  Col_G3 = (TColor)4227327;  //RGB(0,0,255);
  Col_G4 = (TColor)16711680; //RGB(255,92,92);
}

//------------------------- ������ INI-�����: -------------------------------

void __fastcall TMainForm::ReadConfig(void)
{
  TMemIniFile *ini;
  ini = new TMemIniFile(ChangeFileExt(Application->ExeName,".ini" ));
  Left = ini->ReadInteger("General","Left",Left);
  Top = ini->ReadInteger("General","Top",Top);
  Width = ini->ReadInteger("General","Width",Width);
  Height = ini->ReadInteger("General","Height",Height);
  WindowState = TWindowState(ini->ReadInteger("General", "WindowState", WindowState));
  FDataDir = ini->ReadString("General","DataDir",ExtractFilePath(ParamStr(0)));
  cbVgr->Checked = ini->ReadBool("General","ShowV",1);
  cbIgr->Checked = ini->ReadBool("General","ShowI",1);
  cbPgr->Checked = ini->ReadBool("General","ShowP",0);
  cbTgr->Checked = ini->ReadBool("General","ShowT",0);
  cbFgr->Checked = ini->ReadBool("General","ShowFan",0);
  FAutoConnect = ini->ReadBool("Settings","AutoConnect",1);
  FAutoLoad = ini->ReadBool("Settings","AutoLoad",1);
  FSample = ini->ReadInteger("Settings","Sample",100);
  FAutoScroll = ini->ReadBool("Settings","AutoScroll",1);
  Col_Bg = (TColor)ini->ReadInteger("Colours","Background",Col_Bg);
  Col_Ax = (TColor)ini->ReadInteger("Colours","Axis",Col_Ax);
  Col_Tx = (TColor)ini->ReadInteger("Colours","Text",Col_Tx);
  Col_Cr = (TColor)ini->ReadInteger("Colours","Cursor",Col_Cr);
  Col_G1 = (TColor)ini->ReadInteger("Colours","Graph1",Col_G1);
  Col_G2 = (TColor)ini->ReadInteger("Colours","Graph2",Col_G2);
  Col_G3 = (TColor)ini->ReadInteger("Colours","Graph3",Col_G3);
  Col_G4 = (TColor)ini->ReadInteger("Colours","Graph4",Col_G4);
  delete ini;
}

//----------------------- ���������� INI-�����: -----------------------------

void __fastcall TMainForm::SaveConfig(void)
{
  TMemIniFile *ini;
  ini = new TMemIniFile(ChangeFileExt(Application->ExeName,".ini" ));
  ini->WriteInteger("General", "WindowState", WindowState);
  if (WindowState != wsMaximized)
   { ini->WriteInteger("General","Left",Left);
     ini->WriteInteger("General","Top",Top);
     ini->WriteInteger("General","Width",Width);
     ini->WriteInteger("General","Height",Height);
   }  
  ini->WriteString("General","DataDir",FDataDir);
  ini->WriteBool("General","ShowV",cbVgr->Checked);
  ini->WriteBool("General","ShowI",cbIgr->Checked);
  ini->WriteBool("General","ShowP",cbPgr->Checked);
  ini->WriteBool("General","ShowT",cbTgr->Checked);
  ini->WriteBool("General","ShowFan",cbFgr->Checked);
  ini->WriteBool("Settings","AutoConnect",FAutoConnect);
  ini->WriteBool("Settings","AutoRead",FAutoLoad);
  ini->WriteInteger("Settings","Sample",FSample);
  ini->WriteBool("Settings","AutoScroll",FAutoScroll);
  ini->WriteInteger("Colours","Background",Col_Bg);
  ini->WriteInteger("Colours","Axis",Col_Ax);
  ini->WriteInteger("Colours","Text",Col_Tx);
  ini->WriteInteger("Colours","Cursor",Col_Cr);
  ini->WriteInteger("Colours","Graph1",Col_G1);
  ini->WriteInteger("Colours","Graph2",Col_G2);
  ini->WriteInteger("Colours","Graph3",Col_G3);
  ini->WriteInteger("Colours","Graph4",Col_G4);
  try { ini->UpdateFile(); } catch (Exception &E)
   { MessageDlg(E.Message, mtError, TMsgDlgButtons() << mbOK, 0); }
  delete ini;
}

//------------------------ ������ ����� ������: -----------------------------

void __fastcall TMainForm::ReadData(AnsiString Name)
{
  //���������� ���� � ����� �����:
  FDataDir = ExtractFilePath(Name);
  FFileName = ExtractFileName(Name);
  bool result = 1;
  TStringList *in = new TStringList();
  TStringList *line = new TStringList();
  TCursor Save_Cursor = Screen->Cursor;
  try
  {
    Screen->Cursor = crHourGlass;
    try
    {
      //������ ����� � StringList:
      in->LoadFromFile(Name);
      //���� ������ ������ ����� �� �������� ������ "Time",
      //�� ������ ������� �����:
      if(!in->Strings[0].AnsiPos("Time"))
        throw EInOutError("Bad file format");
      acStopExecute(NULL); //��������� ���������� ��������
      Clear(); //������� ��������
      //�������������� ������ ��������, ����������� � �����:
      cbVgr->Checked = in->Strings[0].AnsiPos("V");
      cbIgr->Checked = in->Strings[0].AnsiPos("I");
      cbPgr->Checked = in->Strings[0].AnsiPos("P");
      cbTgr->Checked = in->Strings[0].AnsiPos("T");
      cbFgr->Checked = in->Strings[0].AnsiPos("F");
      //���������� � �������� �����:
      line->Delimiter = '\t';
      for(int i = 1; i < in->Count; i++)
      {
        //���������� �������� ����� �� ������ StringList
        //� ���� DelimitedText:
        line->DelimitedText = in->Strings[i];
        int j = 1; //������ ������� - �����, ����������
        //FSample ����� �� ������ ������� ������ ������:
        if(i == 2) FSample = StrToInt(line->Strings[0]);
        //����������� ������ ������� � int � ��������� � ��������:
        if(cbVgr->Checked) DataV[FPointer] = StrToFloat(line->Strings[j++]);
        if(cbIgr->Checked) DataI[FPointer] = StrToFloat(line->Strings[j++]);
        if(cbPgr->Checked) DataP[FPointer] = StrToFloat(line->Strings[j++]);
        if(cbTgr->Checked) DataT[FPointer] = StrToFloat(line->Strings[j++]);
        if(cbFgr->Checked) DataF[FPointer] = StrToFloat(line->Strings[j++]);
        FPointer++;
      }
    }
    catch (Exception &E)
    {
      MessageDlg("Bad file format.", mtError, TMsgDlgButtons() << mbOK, 0);
      result = 0;
    }
  }
  __finally
  {
    Screen->Cursor = Save_Cursor;
    delete in;
    delete line;
    //����������� ��������:
    RedrawAll();
  }
  if(result) //���� ���� ��� ������ �������,
  {
    //���������� ��������� ����������:
    Caption = SOFTNAME;
    Caption = Caption + " - " + FFileName;
  }
}

//---------------------- ���������� ����� ������: --------------------------

void __fastcall TMainForm::SaveData(AnsiString Name)
{
  //���������� ���� � ����� �����:
  FDataDir = ExtractFilePath(Name);
  FFileName = ExtractFileName(Name);
  //���������� ������ � ����:
  TStringList *out = new TStringList();
  TCursor Save_Cursor = Screen->Cursor;
  bool not_saved = 0;
  try
  {
    Screen->Cursor = crHourGlass;
    //������������ ������ ���������, �� �������
    //����� ����� ����������, ����� ������ ������� ���������:
    AnsiString str = "Time";
    if(cbVgr->Checked) str = str + "\t" + "V";
    if(cbIgr->Checked) str = str + "\t" + "I";
    if(cbPgr->Checked) str = str + "\t" + "P";
    if(cbTgr->Checked) str = str + "\t" + "T";
    if(cbFgr->Checked) str = str + "\t" + "F";
    out->Add(str);
    //���������� ��������:
    for(int i = 0; i < FPointer; i++)
    {
      str = IntToStr(i * FSample);
      if(cbVgr->Checked) str = str + "\t" + FloatToStr(DataV[i]);
      if(cbIgr->Checked) str = str + "\t" + FloatToStr(DataI[i]);
      if(cbPgr->Checked) str = str + "\t" + FloatToStr(DataP[i]);
      if(cbTgr->Checked) str = str + "\t" + FloatToStr(DataT[i]);
      if(cbFgr->Checked) str = str + "\t" + FloatToStr(DataF[i]);
      out->Add(str);
    }
    try
    {
      out->SaveToFile(Name);
    }
    catch (Exception &E)
    {
      MessageDlg("Unable to save file.", mtError, TMsgDlgButtons() << mbOK, 0);
      not_saved = FModified;
    }
  }
  __finally
  {
    Screen->Cursor = Save_Cursor;
    delete out;
  }
  //���������� ��������� ����������:
  Caption = SOFTNAME;
  Caption = Caption + " - " + FFileName;
  FModified = not_saved;
}

//---------------------------------------------------------------------------
//------------------------------ Menus: -------------------------------------
//---------------------------------------------------------------------------

//----------------------------- Menu New: -----------------------------------

void __fastcall TMainForm::acNewExecute(TObject *Sender)
{
  Clear();
  RedrawAll();
  FFileName = "";
  Caption = SOFTNAME;
  FModified = 0;
}

//----------------------------- Menu Open: ----------------------------------

void __fastcall TMainForm::acOpenExecute(TObject *Sender)
{
  //��������� ����������, ����� ����� "���������" Windows
  //����������������� ��������� �� ���������
  DecimalSeparator = '.';
  //���������� ����� �������:
  OpenDialog->Title = "Open Data File";
  OpenDialog->InitialDir = FDataDir;
  OpenDialog->FileName = "";
  OpenDialog->Filter = "Data file (*.dat)|*.dat|All files (*.*)|*.*";
  OpenDialog->DefaultExt = "dat";
  //����� ������� "Open...":
  if(!OpenDialog->Execute()) return;
  //����������� ����� ��� ����� �������:
  Refresh();
  //������ �����:
  ReadData(OpenDialog->FileName);
}

//--------------------------- Menu Save As: ---------------------------------

void __fastcall TMainForm::acSaveasExecute(TObject *Sender)
{
  //��������� ����������, ����� ����� "���������" Windows
  //����������������� ��������� �� ���������
  DecimalSeparator = '.';
  //���������� ����� �������:
  SaveDialog->Title = "Save Data File";
  SaveDialog->InitialDir = FDataDir;
  SaveDialog->FileName = FFileName;
  SaveDialog->Filter = "Data file (*.dat)|*.dat|All files (*.*)|*.*";
  SaveDialog->DefaultExt = "dat";
  //����� ������� "Save As...":
  if(!SaveDialog->Execute()) return;
  //����������� ����� ��� ����� �������:
  Refresh();
  //���������� �����:
  SaveData(SaveDialog->FileName);
}

//---------------------------- Menu Exit: -----------------------------------

void __fastcall TMainForm::acExitExecute(TObject *Sender)
{
  Close();
}

//----------------------------- Connect: ------------------------------------

void __fastcall TMainForm::acConnectExecute(TObject *Sender)
{
  Disconnect();
  Connect();
  if(!FConnected)
    Application->MessageBox("No device found.", "Warning", MB_OK | MB_ICONWARNING);
}

void __fastcall TMainForm::Connect(void)
{
  FConnected = PSL_OpenDevice();
  if(FConnected)
  {
    LPSTR DevName;
    PSL_GetInfo(DevName);
    AnsiString s = DevName;
    StatusBar->Panels->Items[0]->Text = " ON LINE: " + s;
    //�������������� �������� ����������:
    if(FAutoLoad) acReadExecute(NULL);
  }
  else
  {
    Disconnect();
  }
}

//---------------------------- Disconnect: ----------------------------------

void __fastcall TMainForm::acDisconnectExecute(TObject *Sender)
{
  Disconnect();
}

void __fastcall TMainForm::Disconnect(void)
{
  if(FConnected) PSL_SetVI(sbV->Position, sbI->Position, 0);
  PSL_CloseDevice();
  StatusBar->Panels->Items[0]->Text = " OFF LINE ";
}

//---------------------------- Read Params: ---------------------------------

void __fastcall TMainForm::acReadExecute(TObject *Sender)
{
  if(FConnected && !Lock)
  {
    int v = 0, i = 0;
    PSL_GetVI(v, i);
    feV->Value = v / 100.0;
    feI->Value = i / 1000.0;
    sbV->Position = v;
    sbI->Position = i;
  }
}

//--------------------------- Write Params: ---------------------------------

void __fastcall TMainForm::acWriteExecute(TObject *Sender)
{
  if(FConnected)
  {
    PSL_SetVI(sbV->Position, sbI->Position, rbOut->Checked);
  }
}

//---------------------------- Menu Start: ----------------------------------

void __fastcall TMainForm::acStartExecute(TObject *Sender)
{
  if(!acStart->Checked)
  {
    acStart->Checked = 1;
    acPause->Checked = 0;
    acStop->Checked = 0;
    if(FMode == MD_STOP) acNewExecute(NULL);
    FMode = MD_START;
    FModified = 1;
  }
  else
  {
    acStart->Checked = 0;
    acPause->Checked = 1;
    acStop->Checked = 0;
    FMode = MD_PAUSE;
  }
}

//---------------------------- Menu Pause: ----------------------------------

void __fastcall TMainForm::acPauseExecute(TObject *Sender)
{
  if(acStop->Checked) return;
  if(!acPause->Checked)
  {
    acPause->Checked = 1;
    acStart->Checked = 0;
    acStop->Checked = 0;
    FMode = MD_PAUSE;
  }
  else
  {
    acPause->Checked = 0;
    acStart->Checked = 1;
    acStop->Checked = 0;
    FMode = MD_START;
  }
}

//----------------------------- Menu Stop: ----------------------------------

void __fastcall TMainForm::acStopExecute(TObject *Sender)
{
  acStop->Checked = 0;
  acStart->Checked = 0;
  acPause->Checked = 0;
  FMode = MD_STOP;
}

//----------------------------- Menu Freeze: --------------------------------

void __fastcall TMainForm::acFreezeExecute(TObject *Sender)
{
  acFreeze->Checked = !acFreeze->Checked;
  if(!acFreeze->Checked) RedrawAll();
}

//---------------------------- Menu Cursor: ---------------------------------

void __fastcall TMainForm::acCursorExecute(TObject *Sender)
{
  acCursor->Checked = !acCursor->Checked;
  FCursor = acCursor->Checked;
  DrawLogCur(LogPaintBox->Canvas, FCursor, FCursorX);
}

//----------------------------- Cut Left: -----------------------------------

void __fastcall TMainForm::acCutLExecute(TObject *Sender)
{
  if(FCursor)
  {
    int Xo = LogScrollBar->Position + FCursorX;
    if(Xo < FPointer)
    {
      for(int i = 0; i < FPointer - Xo; i++)
      {
        DataV[i] = DataV[i + Xo]; DataV[i + Xo] = 0;
        DataI[i] = DataI[i + Xo]; DataI[i + Xo] = 0;
        DataP[i] = DataP[i + Xo]; DataP[i + Xo] = 0;
        DataT[i] = DataT[i + Xo]; DataT[i + Xo] = 0;
        DataF[i] = DataF[i + Xo]; DataF[i + Xo] = 0;
      }
      FPointer = FPointer - Xo;
      FCursorX = 0;
      RedrawAll();
      LogScrollBar->Position = 0;
      FModified = 1;
    }
  }
}

//---------------------------- Cut Right: -----------------------------------

void __fastcall TMainForm::acCutRExecute(TObject *Sender)
{
  if(FCursor)
  {
    int Xo = LogScrollBar->Position + FCursorX;
    if(Xo < FPointer)
    {
      for(int i = Xo + 1; i < FPointer; i++)
      {
        DataV[i] = 0;
        DataI[i] = 0;
        DataP[i] = 0;
        DataT[i] = 0;
        DataF[i] = 0;
      }
      FPointer = Xo + 1;
      RedrawAll();
      FModified = 1;
    }
  }
}

//---------------------------- Menu Settings: -------------------------------

void __fastcall TMainForm::acSettingsExecute(TObject *Sender)
{
  if(!SettingsForm) SettingsForm = new TSettingsForm(Application);
  SettingsForm->ShowModal();
  Timer1->Interval = FSample;
  //�������������� �������� ����������:
  if(FAutoLoad) acReadExecute(NULL);
  RedrawAll();
}

//---------------------------- Menu Colours: --------------------------------

void __fastcall TMainForm::acColoursExecute(TObject *Sender)
{
  if(!ColoursForm) ColoursForm = new TColoursForm(Application);
  ColoursForm->ShowModal();
}

//---------------------------- Menu About: ----------------------------------

void __fastcall TMainForm::acAboutExecute(TObject *Sender)
{
  if(!AboutForm) AboutForm = new TAboutForm(Application);
  AboutForm->ShowModal();
}

//---------------------------------------------------------------------------
//--------------------------- ����������: -----------------------------------
//---------------------------------------------------------------------------

//---------------------------- ������� V: -----------------------------------

void __fastcall TMainForm::feVChanged(TObject *Sender)
{
  if(!Lock)
  {
    Lock = 1;
    sbV->Position = ROUND(feV->Value * 100);
    PSL_SetVI(sbV->Position, sbI->Position, rbOut->Checked);
    Lock = 0;
  }
}


void __fastcall TMainForm::sbVChange(TObject *Sender)
{
  if(!Lock)
  {
    Lock = 1;
    feV->Value = sbV->Position / 100.0;
    PSL_SetVI(sbV->Position, sbI->Position, rbOut->Checked);
    Lock = 0;
  }
}

//---------------------------- ������� I: -----------------------------------


void __fastcall TMainForm::feIChanged(TObject *Sender)
{
  if(!Lock)
  {
    Lock = 1;
    sbI->Position = ROUND(feI->Value * 1000);
    PSL_SetVI(sbV->Position, sbI->Position, rbOut->Checked);
    Lock = 0;
  }
}

void __fastcall TMainForm::sbIChange(TObject *Sender)
{
  if(!Lock)
  {
    Lock = 1;
    feI->Value = sbI->Position / 1000.0;
    PSL_SetVI(sbV->Position, sbI->Position, rbOut->Checked);
    Lock = 0;
  }
}

//----------------------------- Out ON/OFF: ---------------------------------

void __fastcall TMainForm::btOnClick(TObject *Sender)
{
  rbOut->Checked = 1;
  PSL_SetVI(sbV->Position, sbI->Position, rbOut->Checked);
}

void __fastcall TMainForm::btOffClick(TObject *Sender)
{
  rbOut->Checked = 0;
  PSL_SetVI(sbV->Position, sbI->Position, rbOut->Checked);
}

void __fastcall TMainForm::rbOutClick(TObject *Sender)
{
  PSL_SetVI(sbV->Position, sbI->Position, rbOut->Checked);
}

//---------------------------------------------------------------------------
//--------------------------- ����� ��������: -------------------------------
//---------------------------------------------------------------------------

#define SCALE   40 //����� �� ��� Y
#define X_STEP  50 //��� ����� �� ��� X
#define Y_DIV   20 //���������� ������� �� ��� Y

#define GAPL    32 //����� ����� �� �����
#define GAPR    24 //����� ������ �� �����
#define GAPT    12 //����� ������ �� �����
#define GAPB    22 //����� ����� �� �����

//---------------------------- ����� �����: ---------------------------------

void __fastcall TMainForm::DrawLogGrid(TCanvas *cv)
{
  //������� ������:
  TrsPanel->Color = Col_Bg;
  lbTraces->Font->Color = Col_Tx;
  lbVgr->Font->Color = Col_G1;
  lbIgr->Font->Color = Col_G2;
  lbPgr->Font->Color = Col_G4;
  lbTgr->Font->Color = Col_G2;
  lbFgr->Font->Color = Col_G3;
  //���������� ����������:
  int DeltaX = LogPaintBox->Width - GAPL - GAPR;
  int DeltaY = LogPaintBox->Height - GAPB - GAPT;
  LogScrollBar->Max = LOGN - DeltaX;
  int Xo = LogScrollBar->Position;
  //�������:
  cv->Pen->Mode = pmCopy;
  cv->Brush->Color = Col_Bg;
  cv->FillRect(Rect(0, 0, LogPaintBox->Width, LogPaintBox->Height));
  //��������� ������������ ����� �����:
  cv->Pen->Color = Col_Ax;
  cv->Font->Color = Col_Tx;
  int i = - Xo % X_STEP; if(i < 0) i += X_STEP;
  cv->MoveTo(GAPL, GAPT);
  cv->LineTo(GAPL, GAPT + DeltaY);
  cv->MoveTo(GAPL + DeltaX - 1, GAPT);
  cv->LineTo(GAPL + DeltaX - 1, GAPT + DeltaY);
  AnsiString txt;
  while(i < DeltaX)
  {
    cv->MoveTo(GAPL + i, GAPT);
    cv->LineTo(GAPL + i, GAPT + DeltaY + 3);
    txt = IntToStr((Xo + i) * FSample / 1000);
    int x = GAPL + i - cv->TextWidth(txt) / 2;
    cv->TextOutA(x, GAPT + DeltaY + 5, txt);
    i += X_STEP;
  }
  cv->TextOutA(GAPL + DeltaX + 14, GAPT + DeltaY + 5, "s");
  //��������� �������������� ����� �����:
  cv->Font->Color = Col_Tx;
  txt = "0";
  int h = cv->TextHeight(txt) / 2;
  for(int i = 0; i <= Y_DIV; i++)
  {
    int y = GAPT + (i * (double)DeltaY / Y_DIV + 0.5);
    cv->MoveTo(GAPL - 3, y);
    cv->LineTo(GAPL + DeltaX, y);
    //����� ������ �� Y:
    y = y - h;
    txt = IntToStr((Y_DIV - i) * SCALE / Y_DIV);
    int x = GAPL - cv->TextWidth(txt) - 7;
    cv->TextOutA(x, y, txt);
  }
  FCurOn = 0;
  DrawLogCur(cv, FCursor, FCursorX);
}

//--------------- ��������� ������� � ��������� ���������: ------------------

#define RANGE_V      40  //�������� ������� ����������, �
#define RANGE_I       4  //�������� ������� ����, �
#define RANGE_P     160  //�������� ������� ��������, ��
#define RANGE_T     160  //�������� ������� �����������
#define RANGE_F     160  //�������� ������� ��������

void __fastcall TMainForm::DrawLog(TCanvas *cv, int x1, int x2)
{
  //���������� ����������:
  int DeltaX = LogPaintBox->Width - GAPL - GAPR;
  int DeltaY = LogPaintBox->Height - GAPB - GAPT;
  int Xo = LogScrollBar->Position;
  x1 = x1 - Xo; x2 = x2 - Xo;
  if(x1 < 0) x1 = 0;
  if(x2 > DeltaX - 1) x2 = DeltaX - 1;
  //���������� �������:
  DrawLogCur(cv, 0, FCursorX);
  //��������� ��������:
  cv->Pen->Mode = pmCopy;
  for(int i = x1; i <= x2; i++)
  {
    if(cbVgr->Checked)
      DrawSegment(cv, Col_G1, DataV, RANGE_V, 0, Xo, i, DeltaY);
    if(cbIgr->Checked)
      DrawSegment(cv, Col_G2, DataI, RANGE_I, 0, Xo, i, DeltaY);
    if(cbPgr->Checked)
      DrawSegment(cv, Col_G4, DataP, RANGE_P, 0, Xo, i, DeltaY);
    if(cbTgr->Checked)
      DrawSegment(cv, Col_G2, DataT, RANGE_T, 0, Xo, i, DeltaY);
    if(cbFgr->Checked)
      DrawSegment(cv, Col_G3, DataF, RANGE_F, 0, Xo, i, DeltaY);
  }
  //��������� �������:
  DrawLogCur(cv, FCursor, FCursorX);
}

//--------------------- ��������� �������� �������: -------------------------

void __fastcall TMainForm::DrawSegment(TCanvas *cv, TColor cl, double *data,
                             double range, int shift, int x0, int x, int dy)
{
  cv->Pen->Color = cl;
  double k = double(dy) / range;
  int Y2 = int(k * (data[x0 + x] + shift) + 0.5);
  int Y1 = int(k * (data[x0 + x - ((x)? 1 : 0)] + shift) + 0.5);
  if(Y1 > dy) Y1 = dy;
  if(Y2 > dy) Y2 = dy;
  if(Y1 < 0) Y1 = 0;
  if(Y2 < 0) Y2 = 0;
  if(Y2 < Y1) Y2 = Y2 - 1; else Y2 = Y2 + 1;
  cv->MoveTo(GAPL + x, GAPT + dy - Y1);
  cv->LineTo(GAPL + x, GAPT + dy - Y2);
}

//------------------------- ��������� �������: ------------------------------

void __fastcall TMainForm::DrawLogCur(TCanvas *cv, bool e, int x)
{
  //���������� ����������:
  int DeltaX = LogPaintBox->Width - GAPL - GAPR;
  int DeltaY = LogPaintBox->Height - GAPB - GAPT;
  int Xo = LogScrollBar->Position;
  if (x < 0) x = 0;
  if (x > DeltaX - 1) x = DeltaX - 1;
  cv->Pen->Mode = pmNotXor;
  if(FCurOn)
  {
    //�������� �������:
    cv->Pen->Color = (Col_Cr ^ ~Col_Bg) & 0xFFFFFF;
    cv->MoveTo(GAPL + FCursorX, GAPT + DeltaY);
    cv->LineTo(GAPL + FCursorX, GAPT - 1);
  }
  if(e)
  {
    int j = x + Xo;
    //��������� �������:
    cv->Pen->Color = (Col_Cr ^ ~Col_Bg) & 0xFFFFFF;
    cv->MoveTo(GAPL + x, GAPT + DeltaY);
    cv->LineTo(GAPL + x, GAPT - 1);
    //����� ���������� X:
    StatusBar->Panels->Items[1]->Text = " t = " +
     FloatToStrF(FSample * (j) / 1000.0, ffFixed, 5, 1) + " s ";
    //����� ��������� Y:
    if(cbVgr->Checked)
      StatusBar->Panels->Items[2]->Text =
       FloatToStrF(DataV[j],  ffFixed, 5, 2) + " V";
      else StatusBar->Panels->Items[2]->Text = "";
    if(cbIgr->Checked)
      StatusBar->Panels->Items[3]->Text =
       FloatToStrF(DataI[j],  ffFixed, 5, 3) + " A";
      else StatusBar->Panels->Items[3]->Text = "";
    if(cbPgr->Checked)
      StatusBar->Panels->Items[4]->Text =
       FloatToStrF(DataP[j],  ffFixed, 6, 3) + " W";
      else StatusBar->Panels->Items[4]->Text = "";
    if(cbTgr->Checked)
      StatusBar->Panels->Items[5]->Text =
       FloatToStrF(DataT[j],  ffFixed, 5, 1) + "�C";
      else StatusBar->Panels->Items[5]->Text = "";
    if(cbFgr->Checked)
      StatusBar->Panels->Items[6]->Text =
       FloatToStrF(DataF[j],  ffFixed, 5, 0) + "%";
      else StatusBar->Panels->Items[6]->Text = "";
  }
  else
  {
    //������� ���������, ���� ������ ��������:
    StatusBar->Panels->Items[1]->Text = "";
    StatusBar->Panels->Items[2]->Text = "";
    StatusBar->Panels->Items[3]->Text = "";
    StatusBar->Panels->Items[4]->Text = "";
    StatusBar->Panels->Items[5]->Text = "";
    StatusBar->Panels->Items[6]->Text = "";
  }
  FCurOn = e;
  FCursorX = x;
}

//-------------------- ���������/���������� ��������: -----------------------

void __fastcall TMainForm::cbVgrClick(TObject *Sender)
{
  RedrawAll();
}

//--------------------------- ��������������: -------------------------------

void __fastcall TMainForm::LogScrollBarChange(TObject *Sender)
{
  RedrawAll();
}

//------------------------ ����������� �������: -----------------------------

void __fastcall TMainForm::LogPaintBoxPaint(TObject *Sender)
{
  RedrawAll();
}

//---------------------------- Cursor On: -----------------------------------

void __fastcall TMainForm::LogPaintBoxMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  if(Shift.Contains(ssLeft))
  {
    acCursor->Checked = 1;
    FCursor = 1;
    DrawLogCur(LogPaintBox->Canvas, FCursor, X - GAPL);
  }
}

//--------------------------- Cursor Move: ----------------------------------

void __fastcall TMainForm::LogPaintBoxMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
  if (Shift.Contains(ssLeft))
  {
    //�������������� ��������������:
    if(FAutoScroll)
    {
      if(X > LogPaintBox->Width - GAPR)
        LogScrollBar->Position += X - LogPaintBox->Width + GAPR;
      if(X < GAPL)
        LogScrollBar->Position -= GAPL - X;
    }
    DrawLogCur(LogPaintBox->Canvas, FCursor, X - GAPL);
  }
}

//-------------------------- ������� ������: --------------------------------

void __fastcall TMainForm::Clear(void)
{
  for(int i = 0; i < LOGN; i++)
  {
    DataV[i] = 0;
    DataI[i] = 0;
    DataP[i] = 0;
    DataT[i] = 0;
    DataF[i] = 0;
  }
  FPointer = 0;
  LogScrollBar->Position = 0;
}

//----------------------- ����������� �����: --------------------------------

void __fastcall TMainForm::RedrawAll(void)
{
  Bm->Height = LogPaintBox->Height;
  Bm->Width = LogPaintBox->Width;
  DrawLogGrid(Bm->Canvas);
  if(FPointer) DrawLog(Bm->Canvas, 0, FPointer - 1);
  LogPaintBox->Canvas->Draw(0, 0, Bm);
}

//---------------------------------------------------------------------------
//---------------------------- Timer: ---------------------------------------
//---------------------------------------------------------------------------

void __fastcall TMainForm::Timer1Timer(TObject *Sender)
{
  Timer1->Enabled = 0;
  if(FConnected) //���� ���������� ����������
  {
    int v = 0, i = 0, s = 0, t = 0, f = 0;
    double fv, fi;
    //������ ���������� VI:
    FConnected = PSL_GetVIavg(v, i);
    fv = v / 100.0;
    fi = i / 1000.0;
    edV->Text = FloatToStrF(fv,  ffFixed, 5, 2);
    edI->Text = FloatToStrF(fi,  ffFixed, 5, 3);
    //������ �������:
    if(FConnected) PSL_GetStat(s);
    rbOut->Checked = s & 1;
    rbCV->Checked = s & 2;
    rbCC->Checked = s & 4;
    //������ ������������� VI:
    if(FConnected) PSL_GetVI(v, i);
    Lock = 1;
    feV->Value = v / 100.0;
    feI->Value = i / 1000.0;
    sbV->Position = v;
    sbI->Position = i;
    Lock = 0;
    if(FConnected) PSL_GetFan(f, t);
    if(!FCursor && FMode != MD_START)
      StatusBar->Panels->Items[5]->Text =
       FloatToStrF(t / 10.0,  ffFixed, 5, 1) + "�C";
    //���� �������� ������ �������:
    if(FMode == MD_START)
    {
     if(FPointer < LOGN)
      {
        //���������� �����:
        DataV[FPointer] = fv;
        DataI[FPointer] = fi;
        DataP[FPointer] = fv * fi;
        DataT[FPointer] = t / 10.0;
        DataF[FPointer] = f;
        //��������� �����:
        if(!acFreeze->Checked)
          DrawLog(LogPaintBox->Canvas, FPointer, FPointer);
        FPointer++;
        //�������������� ��������������:
        if(FAutoScroll && !acFreeze->Checked &&
             FPointer - LogScrollBar->Position >
               LogPaintBox->Width - GAPL - GAPR)
          LogScrollBar->Position += 100;
      }
      else
      {
        //���� ����� ��������, ����:
        acStopExecute(NULL);
      }
    }
  }
  if(!FConnected)
  {
    Disconnect();
    //�������������� �����������:
    if(FAutoConnect) Connect();
  }
  Timer1->Enabled = 1;
}

//---------------------------------------------------------------------------

