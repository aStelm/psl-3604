object SettingsForm: TSettingsForm
  Left = 478
  Top = 284
  ActiveControl = btOk
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Settings...'
  ClientHeight = 361
  ClientWidth = 384
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object btCancel: TButton
    Left = 302
    Top = 328
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    OnClick = btCancelClick
  end
  object btOk: TButton
    Left = 218
    Top = 328
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
    OnClick = btOkClick
  end
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 369
    Height = 313
    ActivePage = TabSheet4
    TabIndex = 0
    TabOrder = 3
    object TabSheet4: TTabSheet
      Caption = 'General'
      object GroupBox2: TGroupBox
        Left = 8
        Top = 8
        Width = 345
        Height = 81
        Caption = ' Startup '
        TabOrder = 0
        object cbAutoConnect: TCheckBox
          Left = 16
          Top = 24
          Width = 97
          Height = 17
          Caption = 'Auto connect'
          TabOrder = 0
        end
        object cbAutoLoad: TCheckBox
          Left = 16
          Top = 48
          Width = 121
          Height = 17
          Caption = 'Auto load parameters'
          TabOrder = 1
        end
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 96
        Width = 345
        Height = 81
        Caption = ' Graph '
        TabOrder = 1
        object Label1: TLabel
          Left = 15
          Top = 24
          Width = 59
          Height = 13
          Caption = 'Sample rate:'
          Transparent = True
        end
        object cbAutoScroll: TCheckBox
          Left = 16
          Top = 48
          Width = 97
          Height = 17
          Caption = 'Auto scroll'
          TabOrder = 1
        end
        object cbSample: TComboBox
          Left = 96
          Top = 21
          Width = 65
          Height = 21
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 0
          Text = '20 ms'
          Items.Strings = (
            '20 ms'
            '40 ms'
            '60 ms'
            '80 ms'
            '100 ms'
            '120 ms'
            '140 ms'
            '160 ms'
            '180 ms'
            '200 ms'
            '220 ms'
            '240 ms'
            '260 ms'
            '280 ms'
            '300 ms'
            '320 ms'
            '340 ms'
            '360 ms'
            '380 ms'
            '400 ms'
            '420 ms'
            '440 ms'
            '460 ms'
            '480 ms'
            '500 ms')
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Setup'
      object GroupBox3: TGroupBox
        Left = 8
        Top = 8
        Width = 137
        Height = 129
        Caption = ' Control '
        TabOrder = 0
        object cbPAR_LOCK: TCheckBox
          Left = 16
          Top = 24
          Width = 105
          Height = 17
          Caption = 'Lock Controls'
          TabOrder = 0
          OnClick = cbPAR_LOCKClick
        end
        object cbPAR_TRC: TCheckBox
          Tag = 3
          Left = 16
          Top = 48
          Width = 105
          Height = 17
          Caption = 'Output Track'
          TabOrder = 1
          OnClick = cbPAR_LOCKClick
        end
        object cbPAR_CON: TCheckBox
          Tag = 4
          Left = 16
          Top = 72
          Width = 105
          Height = 17
          Caption = 'Confirm'
          TabOrder = 2
          OnClick = cbPAR_LOCKClick
        end
        object cbPAR_ENR: TCheckBox
          Tag = 11
          Left = 16
          Top = 97
          Width = 105
          Height = 17
          Caption = 'Encoder Reverse'
          TabOrder = 3
          OnClick = cbPAR_LOCKClick
        end
      end
      object GroupBox4: TGroupBox
        Left = 8
        Top = 144
        Width = 137
        Height = 129
        Caption = ' Display '
        TabOrder = 1
        object cbPAR_SET: TCheckBox
          Tag = 5
          Left = 16
          Top = 24
          Width = 105
          Height = 17
          Caption = 'Display Setpoints'
          TabOrder = 0
          OnClick = cbPAR_LOCKClick
        end
        object cbPAR_GET: TCheckBox
          Tag = 6
          Left = 16
          Top = 48
          Width = 105
          Height = 17
          Caption = 'Display Getpoints'
          TabOrder = 1
          OnClick = cbPAR_LOCKClick
        end
        object cbPAR_PRC: TCheckBox
          Tag = 7
          Left = 16
          Top = 72
          Width = 105
          Height = 17
          Caption = 'Preview Current'
          TabOrder = 2
          OnClick = cbPAR_LOCKClick
        end
        object cbPAR_SPL: TCheckBox
          Tag = 12
          Left = 16
          Top = 96
          Width = 105
          Height = 17
          Caption = 'Splash Screen'
          TabOrder = 3
          OnClick = cbPAR_LOCKClick
        end
      end
      object GroupBox6: TGroupBox
        Left = 152
        Top = 64
        Width = 201
        Height = 73
        Caption = ' Meters '
        TabOrder = 3
        object Label3: TLabel
          Left = 11
          Top = 20
          Width = 70
          Height = 13
          Caption = 'V Meter Mode:'
          Transparent = True
        end
        object Label4: TLabel
          Left = 11
          Top = 44
          Width = 70
          Height = 13
          Caption = 'A Meter Mode:'
          Transparent = True
        end
        object cbPAR_APV: TComboBox
          Tag = 13
          Left = 88
          Top = 16
          Width = 81
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 1
          TabOrder = 0
          Text = 'Peak High'
          OnChange = cbPAR_SNDChange
          Items.Strings = (
            'Average'
            'Peak High'
            'Peak Low')
        end
        object cbPAR_APC: TComboBox
          Tag = 14
          Left = 88
          Top = 40
          Width = 81
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 1
          TabOrder = 1
          Text = 'Peak High'
          OnChange = cbPAR_SNDChange
          Items.Strings = (
            'Average'
            'Peak High'
            'Peak Low')
        end
      end
      object GroupBox7: TGroupBox
        Left = 152
        Top = 8
        Width = 201
        Height = 49
        Caption = ' Sound '
        TabOrder = 2
        object Label2: TLabel
          Left = 9
          Top = 20
          Width = 64
          Height = 13
          Caption = 'Sound Mode:'
          Transparent = True
        end
        object cbPAR_SND: TComboBox
          Tag = 10
          Left = 88
          Top = 16
          Width = 81
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 1
          TabOrder = 0
          Text = 'Alarm'
          OnChange = cbPAR_SNDChange
          Items.Strings = (
            'Off'
            'Alarm'
            'On')
        end
      end
      object GroupBox8: TGroupBox
        Left = 152
        Top = 228
        Width = 201
        Height = 45
        Caption = ' Info '
        TabOrder = 5
        object Label11: TLabel
          Left = 14
          Top = 18
          Width = 83
          Height = 13
          Caption = 'Firmware Version:'
          Transparent = True
        end
        object fePAR_INF: TFloatEdit
          Tag = 19
          Left = 120
          Top = 16
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 2
          Precision = 5
          MinMaxCheck = True
          MaxValue = 999
          ValueVisible = True
          AutoSize = False
          ReadOnly = True
          TabOrder = 0
        end
      end
      object GroupBox9: TGroupBox
        Left = 152
        Top = 144
        Width = 201
        Height = 77
        Caption = ' Power '
        TabOrder = 4
        object cbPAR_DNP: TCheckBox
          Tag = 8
          Left = 16
          Top = 24
          Width = 105
          Height = 17
          Caption = 'Down Programmer'
          TabOrder = 0
          OnClick = cbPAR_LOCKClick
        end
        object cbPAR_OUT: TCheckBox
          Tag = 9
          Left = 16
          Top = 48
          Width = 105
          Height = 17
          Caption = 'Output Restore'
          TabOrder = 1
          OnClick = cbPAR_LOCKClick
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Protection'
      ImageIndex = 1
      object GroupBox5: TGroupBox
        Left = 8
        Top = 184
        Width = 185
        Height = 81
        Caption = ' Thermal '
        TabOrder = 2
        object Label7: TLabel
          Left = 10
          Top = 26
          Width = 71
          Height = 13
          Caption = 'Fan Threshold:'
          Transparent = True
        end
        object Label8: TLabel
          Left = 162
          Top = 26
          Width = 11
          Height = 13
          Caption = #176'C'
          Transparent = True
        end
        object Label9: TLabel
          Left = 10
          Top = 50
          Width = 79
          Height = 13
          Caption = 'Alarm Threshold:'
          Transparent = True
        end
        object Label10: TLabel
          Left = 162
          Top = 50
          Width = 11
          Height = 13
          Caption = #176'C'
          Transparent = True
        end
        object fePAR_FAN: TFloatEdit
          Tag = 16
          Left = 104
          Top = 24
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 1
          Precision = 5
          MinMaxCheck = True
          MaxValue = 99
          ValueVisible = True
          OnChanged = fePAR_DELChanged
          AutoSize = False
          TabOrder = 0
        end
        object fePAR_ALA: TFloatEdit
          Tag = 17
          Left = 104
          Top = 48
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 1
          Precision = 5
          MinMaxCheck = True
          MaxValue = 99
          ValueVisible = True
          OnChanged = fePAR_DELChanged
          AutoSize = False
          TabOrder = 1
        end
      end
      object GroupBox10: TGroupBox
        Left = 8
        Top = 8
        Width = 185
        Height = 105
        Caption = ' Thersholds '
        TabOrder = 0
        object Label12: TLabel
          Left = 16
          Top = 26
          Width = 75
          Height = 13
          Caption = 'OVP Threshold:'
          Transparent = True
        end
        object Label13: TLabel
          Left = 160
          Top = 24
          Width = 7
          Height = 13
          Caption = 'V'
          Transparent = True
        end
        object Label14: TLabel
          Left = 16
          Top = 50
          Width = 75
          Height = 13
          Caption = 'OCP Threshold:'
          Transparent = True
        end
        object Label15: TLabel
          Left = 16
          Top = 74
          Width = 75
          Height = 13
          Caption = 'OPP Threshold:'
          Transparent = True
        end
        object Label16: TLabel
          Left = 160
          Top = 48
          Width = 7
          Height = 13
          Caption = 'A'
          Transparent = True
        end
        object Label17: TLabel
          Left = 160
          Top = 72
          Width = 11
          Height = 13
          Caption = 'W'
          Transparent = True
        end
        object feOVP: TFloatEdit
          Left = 104
          Top = 24
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 1
          Precision = 5
          MinMaxCheck = True
          MinValue = -99
          MaxValue = 99
          ValueVisible = True
          OnChanged = feOVPChanged
          AutoSize = False
          TabOrder = 0
        end
        object feOCP: TFloatEdit
          Left = 104
          Top = 48
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 1
          Precision = 5
          MinMaxCheck = True
          MinValue = -99
          MaxValue = 99
          ValueVisible = True
          OnChanged = feOVPChanged
          AutoSize = False
          TabOrder = 1
        end
        object feOPP: TFloatEdit
          Left = 104
          Top = 72
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 1
          Precision = 5
          MinMaxCheck = True
          MinValue = -99
          MaxValue = 99
          ValueVisible = True
          OnChanged = feOVPChanged
          AutoSize = False
          TabOrder = 2
        end
      end
      object GroupBox11: TGroupBox
        Left = 8
        Top = 120
        Width = 185
        Height = 57
        Caption = ' Delay '
        TabOrder = 1
        object Label24: TLabel
          Left = 8
          Top = 26
          Width = 81
          Height = 13
          Caption = 'Protection Delay:'
          Transparent = True
        end
        object Label25: TLabel
          Left = 160
          Top = 26
          Width = 13
          Height = 13
          Caption = 'ms'
          Transparent = True
        end
        object fePAR_DEL: TFloatEdit
          Tag = 15
          Left = 104
          Top = 24
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 1
          Precision = 5
          MinMaxCheck = True
          MaxValue = 999
          ValueVisible = True
          OnChanged = fePAR_DELChanged
          AutoSize = False
          TabOrder = 0
        end
      end
      object GroupBox13: TGroupBox
        Left = 200
        Top = 8
        Width = 153
        Height = 105
        Caption = ' Top values '
        TabOrder = 3
        object Label5: TLabel
          Left = 16
          Top = 26
          Width = 36
          Height = 13
          Caption = 'Max. V:'
          Transparent = True
        end
        object Label6: TLabel
          Left = 120
          Top = 24
          Width = 7
          Height = 13
          Caption = 'V'
          Transparent = True
        end
        object Label26: TLabel
          Left = 16
          Top = 50
          Width = 32
          Height = 13
          Caption = 'Max. I:'
          Transparent = True
        end
        object Label27: TLabel
          Left = 16
          Top = 74
          Width = 36
          Height = 13
          Caption = 'Max. P:'
          Transparent = True
        end
        object Label28: TLabel
          Left = 120
          Top = 48
          Width = 7
          Height = 13
          Caption = 'A'
          Transparent = True
        end
        object Label29: TLabel
          Left = 120
          Top = 72
          Width = 11
          Height = 13
          Caption = 'W'
          Transparent = True
        end
        object feMaxV: TFloatEdit
          Left = 64
          Top = 24
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 1
          Precision = 5
          MinMaxCheck = True
          MinValue = -99
          MaxValue = 99
          ValueVisible = True
          OnChanged = feMaxVChanged
          AutoSize = False
          TabOrder = 0
        end
        object feMaxI: TFloatEdit
          Left = 64
          Top = 48
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 1
          Precision = 5
          MinMaxCheck = True
          MinValue = -99
          MaxValue = 99
          ValueVisible = True
          OnChanged = feMaxVChanged
          AutoSize = False
          TabOrder = 1
        end
        object feMaxP: TFloatEdit
          Left = 64
          Top = 72
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 1
          Precision = 5
          MinMaxCheck = True
          MinValue = -99
          MaxValue = 99
          ValueVisible = True
          OnChanged = feMaxVChanged
          AutoSize = False
          TabOrder = 2
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Presets'
      ImageIndex = 3
      object GroupBox12: TGroupBox
        Left = 8
        Top = 8
        Width = 345
        Height = 265
        Caption = ' Presets values '
        TabOrder = 0
        object Label19: TLabel
          Left = 192
          Top = 26
          Width = 7
          Height = 13
          Caption = 'V'
          Transparent = True
        end
        object Label20: TLabel
          Left = 280
          Top = 26
          Width = 7
          Height = 13
          Caption = 'A'
          Transparent = True
        end
        object Label22: TLabel
          Left = 192
          Top = 49
          Width = 7
          Height = 13
          Caption = 'V'
          Transparent = True
        end
        object Label23: TLabel
          Left = 280
          Top = 49
          Width = 7
          Height = 13
          Caption = 'A'
          Transparent = True
        end
        object Label31: TLabel
          Left = 192
          Top = 72
          Width = 7
          Height = 13
          Caption = 'V'
          Transparent = True
        end
        object Label32: TLabel
          Left = 280
          Top = 72
          Width = 7
          Height = 13
          Caption = 'A'
          Transparent = True
        end
        object Label34: TLabel
          Left = 192
          Top = 95
          Width = 7
          Height = 13
          Caption = 'V'
          Transparent = True
        end
        object Label35: TLabel
          Left = 280
          Top = 95
          Width = 7
          Height = 13
          Caption = 'A'
          Transparent = True
        end
        object Label37: TLabel
          Left = 192
          Top = 118
          Width = 7
          Height = 13
          Caption = 'V'
          Transparent = True
        end
        object Label38: TLabel
          Left = 280
          Top = 118
          Width = 7
          Height = 13
          Caption = 'A'
          Transparent = True
        end
        object Label40: TLabel
          Left = 192
          Top = 141
          Width = 7
          Height = 13
          Caption = 'V'
          Transparent = True
        end
        object Label41: TLabel
          Left = 280
          Top = 141
          Width = 7
          Height = 13
          Caption = 'A'
          Transparent = True
        end
        object Label43: TLabel
          Left = 192
          Top = 164
          Width = 7
          Height = 13
          Caption = 'V'
          Transparent = True
        end
        object Label44: TLabel
          Left = 280
          Top = 164
          Width = 7
          Height = 13
          Caption = 'A'
          Transparent = True
        end
        object Label46: TLabel
          Left = 192
          Top = 187
          Width = 7
          Height = 13
          Caption = 'V'
          Transparent = True
        end
        object Label47: TLabel
          Left = 280
          Top = 187
          Width = 7
          Height = 13
          Caption = 'A'
          Transparent = True
        end
        object Label49: TLabel
          Left = 192
          Top = 210
          Width = 7
          Height = 13
          Caption = 'V'
          Transparent = True
        end
        object Label50: TLabel
          Left = 280
          Top = 210
          Width = 7
          Height = 13
          Caption = 'A'
          Transparent = True
        end
        object Label52: TLabel
          Left = 192
          Top = 233
          Width = 7
          Height = 13
          Caption = 'V'
          Transparent = True
        end
        object Label53: TLabel
          Left = 280
          Top = 233
          Width = 7
          Height = 13
          Caption = 'A'
          Transparent = True
        end
        object fePrv0: TFloatEdit
          Left = 136
          Top = 24
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 2
          Precision = 5
          MinMaxCheck = True
          MaxValue = 36
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 1
        end
        object fePri0: TFloatEdit
          Left = 224
          Top = 24
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 3
          Precision = 5
          MinMaxCheck = True
          MaxValue = 4
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 2
        end
        object fePrv1: TFloatEdit
          Tag = 1
          Left = 136
          Top = 47
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 2
          Precision = 5
          MinMaxCheck = True
          MaxValue = 36
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 4
        end
        object fePri1: TFloatEdit
          Tag = 1
          Left = 224
          Top = 47
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 3
          Precision = 5
          MinMaxCheck = True
          MaxValue = 4
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 5
        end
        object fePrv2: TFloatEdit
          Tag = 2
          Left = 136
          Top = 70
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 2
          Precision = 5
          MinMaxCheck = True
          MaxValue = 36
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 7
        end
        object fePri2: TFloatEdit
          Tag = 2
          Left = 224
          Top = 70
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 3
          Precision = 5
          MinMaxCheck = True
          MaxValue = 4
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 8
        end
        object fePrv3: TFloatEdit
          Tag = 3
          Left = 136
          Top = 93
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 2
          Precision = 5
          MinMaxCheck = True
          MaxValue = 36
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 10
        end
        object fePri3: TFloatEdit
          Tag = 3
          Left = 224
          Top = 93
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 3
          Precision = 5
          MinMaxCheck = True
          MaxValue = 4
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 11
        end
        object fePrv4: TFloatEdit
          Tag = 4
          Left = 136
          Top = 116
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 2
          Precision = 5
          MinMaxCheck = True
          MaxValue = 36
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 13
        end
        object fePri4: TFloatEdit
          Tag = 4
          Left = 224
          Top = 116
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 3
          Precision = 5
          MinMaxCheck = True
          MaxValue = 4
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 14
        end
        object fePrv5: TFloatEdit
          Tag = 5
          Left = 136
          Top = 139
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 2
          Precision = 5
          MinMaxCheck = True
          MaxValue = 36
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 16
        end
        object fePri5: TFloatEdit
          Tag = 5
          Left = 224
          Top = 139
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 3
          Precision = 5
          MinMaxCheck = True
          MaxValue = 4
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 17
        end
        object fePrv6: TFloatEdit
          Tag = 6
          Left = 136
          Top = 162
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 2
          Precision = 5
          MinMaxCheck = True
          MaxValue = 36
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 19
        end
        object fePri6: TFloatEdit
          Tag = 6
          Left = 224
          Top = 162
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 3
          Precision = 5
          MinMaxCheck = True
          MaxValue = 4
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 20
        end
        object fePrv7: TFloatEdit
          Tag = 7
          Left = 136
          Top = 185
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 2
          Precision = 5
          MinMaxCheck = True
          MaxValue = 36
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 22
        end
        object fePri7: TFloatEdit
          Tag = 7
          Left = 224
          Top = 185
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 3
          Precision = 5
          MinMaxCheck = True
          MaxValue = 4
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 23
        end
        object fePrv8: TFloatEdit
          Tag = 8
          Left = 136
          Top = 208
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 2
          Precision = 5
          MinMaxCheck = True
          MaxValue = 36
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 25
        end
        object fePri8: TFloatEdit
          Tag = 8
          Left = 224
          Top = 208
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 3
          Precision = 5
          MinMaxCheck = True
          MaxValue = 4
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 26
        end
        object fePrv9: TFloatEdit
          Tag = 9
          Left = 136
          Top = 231
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 2
          Precision = 5
          MinMaxCheck = True
          MaxValue = 36
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 28
        end
        object fePri9: TFloatEdit
          Tag = 9
          Left = 224
          Top = 231
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 3
          Precision = 5
          MinMaxCheck = True
          MaxValue = 4
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 29
        end
        object btPre0: TButton
          Left = 40
          Top = 24
          Width = 73
          Height = 19
          Caption = 'Preset 0'
          TabOrder = 0
          OnClick = btPre0Click
        end
        object btPre1: TButton
          Tag = 1
          Left = 40
          Top = 47
          Width = 73
          Height = 19
          Caption = 'Preset 1'
          TabOrder = 3
          OnClick = btPre0Click
        end
        object btPre2: TButton
          Tag = 2
          Left = 40
          Top = 70
          Width = 73
          Height = 19
          Caption = 'Preset 2'
          TabOrder = 6
          OnClick = btPre0Click
        end
        object btPre3: TButton
          Tag = 3
          Left = 40
          Top = 93
          Width = 73
          Height = 19
          Caption = 'Preset 3'
          TabOrder = 9
          OnClick = btPre0Click
        end
        object btPre4: TButton
          Tag = 4
          Left = 40
          Top = 116
          Width = 73
          Height = 19
          Caption = 'Preset 4'
          TabOrder = 12
          OnClick = btPre0Click
        end
        object btPre5: TButton
          Tag = 5
          Left = 40
          Top = 139
          Width = 73
          Height = 19
          Caption = 'Preset 5'
          TabOrder = 15
          OnClick = btPre0Click
        end
        object btPre6: TButton
          Tag = 6
          Left = 40
          Top = 162
          Width = 73
          Height = 19
          Caption = 'Preset 6'
          TabOrder = 18
          OnClick = btPre0Click
        end
        object btPre7: TButton
          Tag = 7
          Left = 40
          Top = 185
          Width = 73
          Height = 19
          Caption = 'Preset 7'
          TabOrder = 21
          OnClick = btPre0Click
        end
        object btPre8: TButton
          Tag = 8
          Left = 40
          Top = 208
          Width = 73
          Height = 19
          Caption = 'Preset 8'
          TabOrder = 24
          OnClick = btPre0Click
        end
        object btPre9: TButton
          Tag = 9
          Left = 40
          Top = 231
          Width = 73
          Height = 19
          Caption = 'Preset 9'
          TabOrder = 27
          OnClick = btPre0Click
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Calibration'
      ImageIndex = 4
      object GroupBox14: TGroupBox
        Left = 8
        Top = 8
        Width = 345
        Height = 81
        Caption = ' Voltage DAC '
        TabOrder = 0
        object Label18: TLabel
          Left = 120
          Top = 26
          Width = 7
          Height = 13
          Caption = 'V'
          Transparent = True
        end
        object Label21: TLabel
          Left = 216
          Top = 26
          Width = 16
          Height = 13
          Caption = 'C1:'
          Transparent = True
        end
        object Label30: TLabel
          Left = 120
          Top = 49
          Width = 7
          Height = 13
          Caption = 'V'
          Transparent = True
        end
        object Label33: TLabel
          Left = 216
          Top = 49
          Width = 16
          Height = 13
          Caption = 'C2:'
          Transparent = True
        end
        object Label64: TLabel
          Left = 40
          Top = 26
          Width = 16
          Height = 13
          Caption = 'P1:'
          Transparent = True
        end
        object Label65: TLabel
          Left = 40
          Top = 49
          Width = 16
          Height = 13
          Caption = 'P2:'
          Transparent = True
        end
        object feCalVp1: TFloatEdit
          Left = 64
          Top = 24
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 2
          Precision = 5
          MinMaxCheck = True
          MinValue = 0.01
          MaxValue = 8.99
          Value = 0.01
          ValueVisible = True
          OnChanged = feCalVp1Changed
          AutoSize = False
          TabOrder = 0
        end
        object feCalVc1: TFloatEdit
          Tag = 1
          Left = 240
          Top = 24
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 0
          Precision = 5
          MinMaxCheck = True
          MinValue = 1
          MaxValue = 65520
          Value = 1
          ValueVisible = True
          OnChanged = feCalVp1Changed
          AutoSize = False
          TabOrder = 1
        end
        object feCalVp2: TFloatEdit
          Tag = 2
          Left = 64
          Top = 47
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 2
          Precision = 5
          MinMaxCheck = True
          MinValue = 9
          MaxValue = 36
          Value = 9
          ValueVisible = True
          OnChanged = feCalVp1Changed
          AutoSize = False
          TabOrder = 2
        end
        object feCalVc2: TFloatEdit
          Tag = 3
          Left = 240
          Top = 47
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 0
          Precision = 5
          MinMaxCheck = True
          MinValue = 1
          MaxValue = 65520
          Value = 1
          ValueVisible = True
          OnChanged = feCalVp1Changed
          AutoSize = False
          TabOrder = 3
        end
      end
      object GroupBox15: TGroupBox
        Left = 8
        Top = 96
        Width = 345
        Height = 81
        Caption = ' Current DAC '
        TabOrder = 1
        object Label36: TLabel
          Left = 120
          Top = 26
          Width = 7
          Height = 13
          Caption = 'A'
          Transparent = True
        end
        object Label39: TLabel
          Left = 216
          Top = 26
          Width = 16
          Height = 13
          Caption = 'C1:'
          Transparent = True
        end
        object Label42: TLabel
          Left = 120
          Top = 49
          Width = 7
          Height = 13
          Caption = 'A'
          Transparent = True
        end
        object Label45: TLabel
          Left = 216
          Top = 49
          Width = 16
          Height = 13
          Caption = 'C2:'
          Transparent = True
        end
        object Label48: TLabel
          Left = 40
          Top = 26
          Width = 16
          Height = 13
          Caption = 'P1:'
          Transparent = True
        end
        object Label51: TLabel
          Left = 40
          Top = 49
          Width = 16
          Height = 13
          Caption = 'P2:'
          Transparent = True
        end
        object feCalIp1: TFloatEdit
          Tag = 4
          Left = 64
          Top = 24
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 3
          Precision = 5
          MinMaxCheck = True
          MinValue = 0.001
          MaxValue = 0.989
          Value = 0.001
          ValueVisible = True
          OnChanged = feCalVp1Changed
          AutoSize = False
          TabOrder = 0
        end
        object feCalIc1: TFloatEdit
          Tag = 5
          Left = 240
          Top = 24
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 0
          Precision = 5
          MinMaxCheck = True
          MinValue = 1
          MaxValue = 65520
          Value = 1
          ValueVisible = True
          OnChanged = feCalVp1Changed
          AutoSize = False
          TabOrder = 1
        end
        object feCalIp2: TFloatEdit
          Tag = 6
          Left = 64
          Top = 47
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 3
          Precision = 5
          MinMaxCheck = True
          MinValue = 0.99
          MaxValue = 4
          Value = 0.99
          ValueVisible = True
          OnChanged = feCalVp1Changed
          AutoSize = False
          TabOrder = 2
        end
        object feCalIc2: TFloatEdit
          Tag = 7
          Left = 240
          Top = 47
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 0
          Precision = 5
          MinMaxCheck = True
          MinValue = 1
          MaxValue = 65520
          Value = 1
          ValueVisible = True
          OnChanged = feCalVp1Changed
          AutoSize = False
          TabOrder = 3
        end
        object feCalDef: TFloatEdit
          Tag = 10
          Left = 144
          Top = 47
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 0
          Precision = 5
          MinMaxCheck = True
          MaxValue = 65535
          Value = 1
          ValueVisible = True
          OnChanged = fePrv0Changed
          AutoSize = False
          TabOrder = 4
          Visible = False
        end
      end
      object GroupBox16: TGroupBox
        Left = 8
        Top = 184
        Width = 169
        Height = 81
        Caption = ' Voltage ADC '
        TabOrder = 2
        object Label55: TLabel
          Left = 40
          Top = 26
          Width = 16
          Height = 13
          Caption = 'C1:'
          Transparent = True
        end
        object Label57: TLabel
          Left = 40
          Top = 49
          Width = 16
          Height = 13
          Caption = 'C2:'
          Transparent = True
        end
        object feCalVm1: TFloatEdit
          Tag = 9
          Left = 64
          Top = 24
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 0
          Precision = 5
          MinMaxCheck = True
          MinValue = 1
          MaxValue = 65520
          Value = 1
          ValueVisible = True
          OnChanged = feCalVp1Changed
          AutoSize = False
          TabOrder = 0
        end
        object feCalVm2: TFloatEdit
          Tag = 10
          Left = 64
          Top = 47
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 0
          Precision = 5
          MinMaxCheck = True
          MinValue = 1
          MaxValue = 65520
          Value = 1
          ValueVisible = True
          OnChanged = feCalVp1Changed
          AutoSize = False
          TabOrder = 1
        end
      end
      object GroupBox17: TGroupBox
        Left = 184
        Top = 184
        Width = 169
        Height = 81
        Caption = ' Current ADC '
        TabOrder = 3
        object Label54: TLabel
          Left = 40
          Top = 26
          Width = 16
          Height = 13
          Caption = 'C1:'
          Transparent = True
        end
        object Label56: TLabel
          Left = 40
          Top = 49
          Width = 16
          Height = 13
          Caption = 'C2:'
          Transparent = True
        end
        object feCalIm1: TFloatEdit
          Tag = 11
          Left = 64
          Top = 24
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 0
          Precision = 5
          MinMaxCheck = True
          MinValue = 1
          MaxValue = 65520
          Value = 1
          ValueVisible = True
          OnChanged = feCalVp1Changed
          AutoSize = False
          TabOrder = 0
        end
        object feCalIm2: TFloatEdit
          Tag = 12
          Left = 64
          Top = 47
          Width = 49
          Height = 19
          Format = ffFixed
          Digits = 0
          Precision = 5
          MinMaxCheck = True
          MinValue = 1
          MaxValue = 65520
          Value = 1
          ValueVisible = True
          OnChanged = feCalVp1Changed
          AutoSize = False
          TabOrder = 1
        end
      end
    end
  end
  object btReload: TButton
    Left = 134
    Top = 328
    Width = 75
    Height = 25
    Caption = 'Reload'
    TabOrder = 0
    OnClick = btReloadClick
  end
end
