#include <windows.h>
#pragma hdrstop

#include "Wake.h"
#include "Ftdio.h"

#pragma package(smart_init)

//---------------------------- Constants: -----------------------------------

#define CRC_Init 0xDE //CRC Initial value

const unsigned char
 FEND  = 0xC0,        //Frame END
 FESC  = 0xDB,        //Frame ESCape
 TFEND = 0xDC,        //Transposed Frame END
 TFESC = 0xDD;        //Transposed Frame ESCape

//----------------------- Functions prototypes: -----------------------------

void __fastcall DowCRC(unsigned char b,
                       unsigned char &crc);

//--------------------------- Calculate CRC: --------------------------------

void __fastcall DowCRC(unsigned char b, unsigned char &crc)
{ for (int i = 0; i < 8; i++)
  {
   if (((b ^ crc) & 1) != 0)
     crc = ((crc ^ 0x18) >> 1) | 0x80;
       else crc = (crc >> 1) & ~0x80;
   b = b >> 1;
   }
}

//--------------------------- Transmit frame: -------------------------------

bool __fastcall TxFrame(unsigned char ADDR,
                        unsigned char CMD,
                        unsigned char N,
                        unsigned char *Data)
{ unsigned char Buff[518]; DWORD j = 0;
  unsigned char d, crc = CRC_Init;              //init CRC

  for (int i = -4; i <= N; i++)
   {
    if ((i == -3) && (!ADDR)) i++;
    if (i == -4) d = FEND;        else          //FEND
    if (i == -3) d = ADDR & 0x7F; else          //address
    if (i == -2) d = CMD  & 0x7F; else          //command
    if (i == -1) d = N;           else          //N
    if (i ==  N) d = crc;         else          //CRC
                 d = Data[i];                   //data
    DowCRC(d, crc);
    if (i == -3) d = d | 0x80;
    if (i > -4)
     if ((d == FEND) || (d == FESC))
      { Buff[j++] = FESC;
        if (d == FEND) d = TFEND;
                  else d = TFESC;
      }
    Buff[j++] = d;
   }
  return (TxBuff(j, Buff));
}

//--------------------------- Receive frame: --------------------------------

bool __fastcall RxFrame(DWORD To,
                        unsigned char &ADD,
                        unsigned char &CMD,
                        unsigned char &N,
                        unsigned char *Data)
{ int i;
  unsigned char b = 0, crc = CRC_Init;          //init CRC
  if (!SetRxTo(To)) return false;               //set timeouts error
  for (i = 0; i < 512 && b != FEND; i++)
   if (!RxByte(b))     break;                   //frame synchronzation
  if (b != FEND) return false;                  //timeout or sync error
  DowCRC(b, crc);                               //update CRC
  N = 0; ADD = 0;
  for (i = -3; i<=N; i++)
   {
    if (!RxByte(b))    break;                   //timeout error
    if (b == FESC)
     if (!RxByte(b))   break;                   //timeout error
      else
       { if (b == TFEND) b = FEND;              //TFEND <- FEND
          else
         if (b == TFESC) b = FESC;              //TFESC <- FESC
          else break;
       }
    if (i == -3)
     if ((b & 0x80) == 0) { CMD = b; i++; }     //CMD (b.7=0)
      else { b = b & 0x7F; ADD = b; }           //ADD (b.7=1)
     else
    if (i == -2)
     if ((b & 0x80) != 0) break;                //CMD error (b.7=1)
      else CMD = b;                             //CMD
     else
    if (i == -1)
     N = b;                                     //N
     else
    if (i !=  N) Data[i] = b;                   //data
    DowCRC(b, crc);                             //update CRC
   }
  return ((i==N+1) && !crc);                    //RX or CRC error
}

//---------------------------------------------------------------------------
