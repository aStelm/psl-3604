#ifndef FTDIO_H
#define FTDIO_H

bool __fastcall ListDevices(DWORD &numDevs);
bool __fastcall OpenPort(int Dev, HANDLE &devHandle);
bool __fastcall SetupPort(DWORD Baud);
bool __fastcall ClosePort(HANDLE &devHandle);
bool __fastcall TxBuff(DWORD j, unsigned char *buff);
bool __fastcall RxByte(unsigned char &b);
bool __fastcall SetRxTo(DWORD to);

#endif

