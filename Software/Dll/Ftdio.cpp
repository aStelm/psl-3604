#include <windows.h>
#pragma hdrstop

#include "Ftdio.h"
#include "Ftd2xx.h"

#pragma package(smart_init)

//------------------------------ Constants: ---------------------------------

#define TIMEOUT 100 //timeout, mS

//------------------------------ Variables: ---------------------------------

FT_HANDLE ftHandle; //device handle
FT_STATUS ftStatus; //device status

//----------------------------- List devices: -------------------------------

bool __fastcall ListDevices(DWORD &numDevs)
{
  ftStatus = FT_ListDevices(&numDevs, NULL, FT_LIST_NUMBER_ONLY);
  return(FT_SUCCESS(ftStatus));
}

//------------------------------ Open port: ---------------------------------

bool __fastcall OpenPort(int Dev, HANDLE &devHandle)
{
  ftStatus = FT_Open(Dev, &ftHandle);
  devHandle = ftHandle;
  return(FT_SUCCESS(ftStatus));
}

//------------------------------ Open port: ---------------------------------

bool __fastcall SetupPort(DWORD Baud)
{
  ftStatus = FT_SetLatencyTimer(ftHandle, 2);
    if(!FT_SUCCESS(ftStatus)) return false;  //set latency error
  ftStatus = FT_SetBaudRate(ftHandle, Baud);
    if(!FT_SUCCESS(ftStatus)) return false;  //set baud error
  ftStatus = FT_SetDataCharacteristics(ftHandle,
               FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_NONE);
    if(!FT_SUCCESS(ftStatus)) return false;  //set mode error
  ftStatus = FT_SetTimeouts(ftHandle, TIMEOUT, TIMEOUT);
    if(!FT_SUCCESS(ftStatus)) return false;  //set timeouts error
  ftStatus = FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX);
    if(!FT_SUCCESS(ftStatus)) return false;  //purge error
  return true;
}

//------------------------------ Close port: --------------------------------

bool __fastcall ClosePort(HANDLE &devHandle)
{
  ftStatus = FT_Close(ftHandle);
  devHandle = NULL;
  return(FT_SUCCESS(ftStatus));
}

//---------------------------- Transmit buffer: -----------------------------

bool __fastcall TxBuff(DWORD j, unsigned char *buff)
{ DWORD r;
  ftStatus = FT_Write(ftHandle, buff, j, &r);
    if(!FT_SUCCESS(ftStatus)) return false;
    if(r != j) return false;
  return true;
}

//------------------------------ Receive byte: ------------------------------

bool __fastcall RxByte(unsigned char &b)
{ DWORD r;
  ftStatus = FT_Read(ftHandle, &b, 1, &r);
    if(!FT_SUCCESS(ftStatus)) return false;
    if(r != 1) return false;
  return true;
}

//-------------------------- Set receive timeout: ---------------------------

bool __fastcall SetRxTo(DWORD to)
{
  ftStatus = FT_SetTimeouts(ftHandle, to, TIMEOUT);
  return(FT_SUCCESS(ftStatus));
}

//---------------------------------------------------------------------------

