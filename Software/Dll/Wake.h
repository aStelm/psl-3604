#ifndef WakeH
#define WakeH

bool __fastcall TxFrame(unsigned char ADDR,
                        unsigned char CMD,
                        unsigned char N,
                        unsigned char *Data);
bool __fastcall RxFrame(DWORD To,
                        unsigned char &ADD,
                        unsigned char &CMD,
                        unsigned char &N,
                        unsigned char *Data);

#endif
