#include <windows.h>
#define PSL_Exports
#include "Main.h"
#include "Ftdio.h"
#include "Wake.h"

typedef char SStr[32];

//---------------------------- Constants: -----------------------------------

//Wake protocol constantes:

#define FRAME     250 //max frame size
#define N_MAX     100 //max word count in frame

//Wake protocol errors:

#define ERR_NO   0    //no error
#define ERR_TX   1    //IO error
#define ERR_BU   2    //device busy error
#define ERR_RE   3    //device not ready
#define ERR_PA   4    //parameters value error
#define ERR_NR   5    //device not responding

//Unknown error:

#define ERR_UK   6    //unknown error

#define MAX_ERR  ERR_UK

//Errors names:

const SStr ErrorMsg[MAX_ERR + 1] =
  { "",
    "invalid packet.",
    "device busy.",
    "device not ready.",
    "invalid parameters.",
    "device not responding.",
    "unknown." };

//Wake protocol commands:

#define CMD_NOP        0 //no operation
#define CMD_ERR        1 //get error code
#define CMD_ECHO       2 //get echo
#define CMD_INFO       3 //get information
#define CMD_SETADDR    4 //set net address
#define CMD_GETADDR    5 //get net address

//Specific commands:

#define CMD_SET_VI       6
#define CMD_GET_VI       7
#define CMD_GET_STAT     8
#define CMD_GET_VI_AVG   9
#define CMD_GET_VI_FAST 10
#define CMD_SET_VIP_MAX 11
#define CMD_GET_VIP_MAX 12
#define CMD_SET_PROT    13
#define CMD_GET_PROT    14
#define CMD_SET_PRE     15
#define CMD_GET_PRE     16
#define CMD_SET_PAR     17
#define CMD_GET_PAR     18
#define CMD_GET_FAN     19
#define CMD_SET_DAC     20
#define CMD_GET_ADC     21
#define CMD_SET_CAL     22
#define CMD_GET_CAL     23
#define CMD_UNKNOWN     24 //unknown command

#define MAX_CMD CMD_UNKNOWN

//Commands names:

const SStr CmdNames[MAX_CMD + 1] =
  { "",
    "IO error: ",
    "Get echo error: ",
    "Get info error: ",
    "Set address error: ",
    "Get address error: ",
    "Set VI error: ",
    "Get VI error: ",
    "Get status error: ",
    "Get VI Avg error: ",
    "Get VI Fast error: ",
    "Set VIP max error: ",
    "Get VIP max error: ",
    "Set prot error: ",
    "Get prot error: ",
    "Set preset error: ",
    "Get preset error: ",
    "Set param. error: ",
    "Get param. error: ",
    "Get fan error: ",
    "Set DAC error: ",
    "Get ADC error: ",
    "Set calib. error: ",
    "Get calib. error: ",
    "System error: " };

const SStr  DevName = "PSL-3604\0"; //device name
const DWORD Baud = 19200;           //baud rate
const ULONG RxTo = 100;             //RX timeout, mS

#define LO(x)    ((unsigned char)(x & 0xFF))
#define HI(x)    ((unsigned char)((x >> 8) & 0xFF))
#define BYTE1(x) ((unsigned char)(x & 0xFF))
#define BYTE2(x) ((unsigned char)((x >> 8) & 0xFF))
#define BYTE3(x) ((unsigned char)((x >> 16) & 0xFF))
#define BYTE4(x) ((unsigned char)((x >> 24) & 0xFF))
#define WORD(lo, hi) ((hi << 8) | lo)

//------------------------------ Variables: ---------------------------------

HANDLE DevHandle;            //device handle
CRITICAL_SECTION Cs;         //critical section
unsigned char TxData[FRAME]; //TX data buffer
unsigned char RxData[FRAME]; //RX data buffer
unsigned char addr, cmd, fn; //Wake protocol variables
char LastError[64] = "";     //last error string

//--------------------- Functions prototypes: -------------------------------

bool __fastcall CmdExe(unsigned char cmd, unsigned char n);
bool __fastcall ErrorReport(int cmd, int errc);

//---------------------------------------------------------------------------

#pragma argsused

int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void* lpReserved)
{
  switch(reason)
  {
    case DLL_PROCESS_ATTACH: InitializeCriticalSection(&Cs); break;
    case DLL_PROCESS_DETACH: DeleteCriticalSection(&Cs); break;
  }
  return 1;
}

//-------------------------- Execute command: -------------------------------

bool __fastcall CmdExe(unsigned char cmd, unsigned char n)
{
  setmem(RxData, FRAME, 0);                    //clear RX data
  if(DevHandle)                                //if port is opened
   {
     if(TxFrame(0, cmd, n, TxData) &&          //transmit frame
        RxFrame(RxTo, addr, cmd, fn, RxData))  //receive frame
          return(ErrorReport(cmd, RxData[0])); //cuccess
     return(ErrorReport(CMD_ERR, ERR_NR));     //device is not respondong
   }
  return(ErrorReport(CMD_NOP, ERR_NO));        //port is closed, return
}

//-------------------------- Report of error: -------------------------------

bool __fastcall ErrorReport(int cmd, int errc)
{
  if(cmd == CMD_ECHO || cmd == CMD_INFO) errc = ERR_NO; //no error code
  if(errc == ERR_NO) cmd = CMD_NOP;  //if no error, empty command string
  if(cmd > MAX_CMD) cmd = MAX_CMD;   //check for command code out of range
  if(errc > MAX_ERR) errc = MAX_ERR; //check for error code out of range
  strcpy(LastError, CmdNames[cmd]);  //copy command name
  strcat(LastError, ErrorMsg[errc]); //copy error message
  return(errc == ERR_NO);
}

//---------------------------------------------------------------------------
//-------------------------- Exported functions -----------------------------
//---------------------------------------------------------------------------

//---------------------------- Open device: ---------------------------------

bool WINAPI PSL_OpenDevice(void)
{
  DWORD numDevs;
  LPCSTR DevInfo;
  if(!ListDevices(numDevs)) return(0);
  for(DWORD i = 0; i < numDevs; i++)
  {
    if(OpenPort(i, DevHandle))
      if(SetupPort(Baud))
        if(PSL_GetInfo(DevInfo))
          if(!strncmp(DevInfo, DevName, 8))
          {
            return(1);
          }
    if(DevHandle)
    {
      ClosePort(DevHandle);
    }
  }
  return(0);
}

//---------------------------- Close device: --------------------------------

bool WINAPI PSL_CloseDevice(void)
{
  return(ClosePort(DevHandle));
}

//-------------------------- Get device handle: -----------------------------

HANDLE WINAPI PSL_GetDeviceHandle(void)
{
  return(DevHandle);
}

//------------------------- Get last error name: ----------------------------

void WINAPI PSL_GetLastError(LPCSTR &lpcStr)
{
  lpcStr = LastError;
}

//------------------------------ CMD_INFO: ----------------------------------

bool WINAPI PSL_GetInfo(LPCSTR &lpcStr)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_INFO);
  bool rx = CmdExe(cmd, 0);
  lpcStr = (LPCSTR)RxData;
  LeaveCriticalSection(&Cs);
  return(rx);
}

//----------------------------- CMD_SET_VI: ---------------------------------

bool WINAPI PSL_SetVI(int v, int i, bool on)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_SET_VI);
  unsigned char n = 0;
  TxData[n++] = LO(v);
  TxData[n++] = HI(v);
  TxData[n++] = LO(i);
  TxData[n++] = HI(i);
  TxData[n++] = on? 1 : 0;
  bool rx = CmdExe(cmd, n);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//---------------------------- CMD_GET_VI: ----------------------------------

bool WINAPI PSL_GetVI(int &v, int &i)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_GET_VI);
  bool rx = CmdExe(cmd, 0);
  unsigned char lo, hi, n = 1;
  lo = RxData[n++]; hi = RxData[n++]; v = WORD(lo, hi);
  lo = RxData[n++]; hi = RxData[n++]; i = WORD(lo, hi);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//---------------------------- CMD_GET_STAT: --------------------------------

bool WINAPI PSL_GetStat(int &s)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_GET_STAT);
  bool rx = CmdExe(cmd, 0);
  unsigned char n = 1;
  s = RxData[n++];
  LeaveCriticalSection(&Cs);
  return(rx);
}

//-------------------------- CMD_GET_VI_AVG: --------------------------------

bool WINAPI PSL_GetVIavg(int &v, int &i)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_GET_VI_AVG);
  bool rx = CmdExe(cmd, 0);
  unsigned char lo, hi, n = 1;
  lo = RxData[n++];
  hi = RxData[n++];
  v = WORD(lo, hi);
  lo = RxData[n++]; hi = RxData[n++]; i = WORD(lo, hi);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//------------------------- CMD_GET_VI_FAST: --------------------------------

bool WINAPI PSL_GetVIfast(int &v, int &i)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_GET_VI_FAST);
  bool rx = CmdExe(cmd, 0);
  unsigned char lo, hi, n = 1;
  lo = RxData[n++]; hi = RxData[n++]; v = WORD(lo, hi);
  lo = RxData[n++]; hi = RxData[n++]; i = WORD(lo, hi);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//-------------------------- CMD_SET_VIP_MAX: -------------------------------

bool WINAPI PSL_SetVIPmax(int v, int i, int p)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_SET_VIP_MAX);
  unsigned char n = 0;
  TxData[n++] = LO(v);
  TxData[n++] = HI(v);
  TxData[n++] = LO(i);
  TxData[n++] = HI(i);
  TxData[n++] = LO(p);
  TxData[n++] = HI(p);
  bool rx = CmdExe(cmd, n);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//------------------------- CMD_GET_VIP_MAX: --------------------------------

bool WINAPI PSL_GetVIPmax(int &v, int &i, int &p)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_GET_VIP_MAX);
  bool rx = CmdExe(cmd, 0);
  unsigned char lo, hi, n = 1;
  lo = RxData[n++]; hi = RxData[n++]; v = WORD(lo, hi);
  lo = RxData[n++]; hi = RxData[n++]; i = WORD(lo, hi);
  lo = RxData[n++]; hi = RxData[n++]; p = WORD(lo, hi);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//--------------------------- CMD_SET_PROT: ---------------------------------

bool WINAPI PSL_SetProt(int v, int i, int p)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_SET_PROT);
  unsigned char n = 0;
  TxData[n++] = LO(v);
  TxData[n++] = HI(v);
  TxData[n++] = LO(i);
  TxData[n++] = HI(i);
  TxData[n++] = LO(p);
  TxData[n++] = HI(p);
  bool rx = CmdExe(cmd, n);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//--------------------------- CMD_GET_PROT: ---------------------------------

bool WINAPI PSL_GetProt(int &v, int &i, int &p)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_GET_PROT);
  bool rx = CmdExe(cmd, 0);
  unsigned char lo, hi, n = 1;
  lo = RxData[n++]; hi = RxData[n++]; v = WORD(lo, hi);
  lo = RxData[n++]; hi = RxData[n++]; i = WORD(lo, hi);
  lo = RxData[n++]; hi = RxData[n++]; p = WORD(lo, hi);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//---------------------------- CMD_SET_PRE: ---------------------------------

bool WINAPI PSL_SetPre(int p, int v, int i)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_SET_PRE);
  unsigned char n = 0;
  TxData[n++] = LO(p);
  TxData[n++] = LO(v);
  TxData[n++] = HI(v);
  TxData[n++] = LO(i);
  TxData[n++] = HI(i);
  bool rx = CmdExe(cmd, n);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//---------------------------- CMD_GET_PRE: ---------------------------------

bool WINAPI PSL_GetPre(int p, int &v, int &i)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_GET_PRE);
  unsigned char n = 0;
  TxData[n++] = LO(p);
  bool rx = CmdExe(cmd, n);
  unsigned char lo, hi;
  n = 1;
  lo = RxData[n++]; hi = RxData[n++]; v = WORD(lo, hi);
  lo = RxData[n++]; hi = RxData[n++]; i = WORD(lo, hi);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//---------------------------- CMD_SET_PAR: ---------------------------------

bool WINAPI PSL_SetPar(int p, int v)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_SET_PAR);
  unsigned char n = 0;
  TxData[n++] = LO(p);
  TxData[n++] = LO(v);
  TxData[n++] = HI(v);
  bool rx = CmdExe(cmd, n);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//---------------------------- CMD_GET_PAR: ---------------------------------

bool WINAPI PSL_GetPar(int p, int &v)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_GET_PAR);
  unsigned char n = 0;
  TxData[n++] = LO(p);
  bool rx = CmdExe(cmd, n);
  unsigned char lo, hi;
  n = 1;
  lo = RxData[n++]; hi = RxData[n++]; v = WORD(lo, hi);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//---------------------------- CMD_GET_FAN: ---------------------------------

bool WINAPI PSL_GetFan(int &s, int &t)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_GET_FAN);
  bool rx = CmdExe(cmd, 0);
  unsigned char lo, hi, n = 1;
  s = RxData[n++];
  lo = RxData[n++]; hi = RxData[n++]; t = WORD(lo, hi);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//---------------------------- CMD_SET_DAC: ---------------------------------

bool WINAPI PSL_SetDAC(int cv, int ci)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_SET_DAC);
  unsigned char n = 0;
  TxData[n++] = LO(cv);
  TxData[n++] = HI(cv);
  TxData[n++] = LO(ci);
  TxData[n++] = HI(ci);
  bool rx = CmdExe(cmd, n);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//---------------------------- CMD_GET_ADC: ---------------------------------

bool WINAPI PSL_GetADC(int &cv, int &ci)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_GET_ADC);
  bool rx = CmdExe(cmd, 0);
  unsigned char lo, hi, n = 1;
  lo = RxData[n++]; hi = RxData[n++]; cv = WORD(lo, hi);
  lo = RxData[n++]; hi = RxData[n++]; ci = WORD(lo, hi);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//---------------------------- CMD_SET_CAL: ---------------------------------

bool WINAPI PSL_SetCal(int c, int v)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_SET_CAL);
  unsigned char n = 0;
  TxData[n++] = LO(c);
  TxData[n++] = LO(v);
  TxData[n++] = HI(v);
  bool rx = CmdExe(cmd, n);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//---------------------------- CMD_GET_CAL: ---------------------------------

bool WINAPI PSL_GetCal(int c, int &v)
{
  EnterCriticalSection(&Cs);
  cmd = LO(CMD_GET_CAL);
  unsigned char n = 0;
  TxData[n++] = LO(c);
  bool rx = CmdExe(cmd, n);
  unsigned char lo, hi;
  n = 1;
  lo = RxData[n++]; hi = RxData[n++]; v = WORD(lo, hi);
  LeaveCriticalSection(&Cs);
  return(rx);
}

//---------------------------------------------------------------------------












