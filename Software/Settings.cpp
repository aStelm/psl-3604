#include <vcl.h>
#pragma hdrstop

#include "Settings.h"
#include "Main.h"

#pragma package(smart_init)
#pragma link "FloatEdit"
#pragma resource "*.dfm"
TSettingsForm *SettingsForm;

//---------------------------------------------------------------------------

__fastcall TSettingsForm::TSettingsForm(TComponent* Owner)
  : TForm(Owner)
{
}

//------------------------------- ReadPar: ----------------------------------

int __fastcall TSettingsForm::ReadPar(int n)
{
  int v;
  PSL_GetPar(n, v);
  return(v);
}

//----------------------------- Form create: --------------------------------

void __fastcall TSettingsForm::FormCreate(TObject *Sender)
{
  cbAutoConnect->Checked = MainForm->FAutoConnect;
  cbAutoLoad->Checked = MainForm->FAutoLoad;
  cbSample->ItemIndex = (MainForm->FSample - 20) / 20;
  cbAutoScroll->Checked = MainForm->FAutoScroll;
  PreV[0] = fePrv0; PreI[0] = fePri0;
  PreV[1] = fePrv1; PreI[1] = fePri1;
  PreV[2] = fePrv2; PreI[2] = fePri2;
  PreV[3] = fePrv3; PreI[3] = fePri3;
  PreV[4] = fePrv4; PreI[4] = fePri4;
  PreV[5] = fePrv5; PreI[5] = fePri5;
  PreV[6] = fePrv6; PreI[6] = fePri6;
  PreV[7] = fePrv7; PreI[7] = fePri7;
  PreV[8] = fePrv8; PreI[8] = fePri8;
  PreV[9] = fePrv9; PreI[9] = fePri9;
  Calib[0] = feCalVp1; Calib[1] = feCalVc1;
  Calib[2] = feCalVp2; Calib[3] = feCalVc2;
  Calib[4] = feCalIp1; Calib[5] = feCalIc1;
  Calib[6] = feCalIp2; Calib[7] = feCalIc2; Calib[8] = feCalDef;
  Calib[9] = feCalVm1; Calib[10] = feCalVm2;
  Calib[11] = feCalIm1; Calib[12] = feCalIm2;
  btReloadClick(NULL);
}

//----------------------------- Form close: ---------------------------------

void __fastcall TSettingsForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  if(ModalResult == mrOk)
  {
    MainForm->FAutoConnect = cbAutoConnect->Checked;
    MainForm->FAutoLoad = cbAutoLoad->Checked;
    MainForm->FSample = cbSample->ItemIndex * 20 + 20;
    MainForm->FAutoScroll = cbAutoScroll->Checked;
  }
  Action = caFree;
  SettingsForm = NULL;
}

//--------------------------- Set Default: ----------------------------------

void __fastcall TSettingsForm::btDefaultClick(TObject *Sender)
{
  cbAutoConnect->Checked = 1;
  cbAutoLoad->Checked = 1;
  cbSample->ItemIndex = (100 - 20) / 20;
  cbAutoScroll->Checked = 1;
}

//------------------------------- ESC: --------------------------------------

void __fastcall TSettingsForm::FormKeyPress(TObject *Sender, char &Key)
{
  if (Key == VK_ESCAPE)
   ModalResult = mrCancel;
}

//------------------------------ Cancel: ------------------------------------

void __fastcall TSettingsForm::btCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}

//-------------------------------- OK: --------------------------------------

void __fastcall TSettingsForm::btOkClick(TObject *Sender)
{
  ModalResult = mrOk;
}

//----------------------------- Check Box: ----------------------------------

void __fastcall TSettingsForm::cbPAR_LOCKClick(TObject *Sender)
{
  if(!Lock)
  {
    TCheckBox *cb = (TCheckBox*)Sender;
    if(MainForm->FConnected)
      PSL_SetPar(cb->Tag, cb->Checked);
  }
}

//----------------------------- Combo Box: ----------------------------------

void __fastcall TSettingsForm::cbPAR_SNDChange(TObject *Sender)
{
  if(!Lock)
  {
    TComboBox *cb = (TComboBox*)Sender;
    if(MainForm->FConnected)
      PSL_SetPar(cb->Tag, cb->ItemIndex);
  }
}

//----------------------------- Float Edit: ---------------------------------

void __fastcall TSettingsForm::fePAR_DELChanged(TObject *Sender)
{
  if(!Lock)
  {
    TFloatEdit *fe = (TFloatEdit*)Sender;
    int v = ROUND(fe->Value);
    if(fe->Tag == PAR_FAN || fe->Tag == PAR_ALA) v = ROUND(v * 10);
    if(MainForm->FConnected)
      PSL_SetPar(fe->Tag, v);
  }
}

//---------------------------- OVP/OCP/OPP: ---------------------------------

void __fastcall TSettingsForm::feOVPChanged(TObject *Sender)
{
  if(!Lock)
  {
    if(MainForm->FConnected)
      PSL_SetProt(ROUND(feOVP->Value * 100),
                  ROUND(feOCP->Value * 1000),
                  ROUND(feOPP->Value * 10));
  }
}

//-------------------------------- TOP: -------------------------------------


void __fastcall TSettingsForm::feMaxVChanged(TObject *Sender)
{
  if(!Lock)
  {
    if(MainForm->FConnected)
     PSL_SetVIPmax(ROUND(feMaxV->Value * 100),
                   ROUND(feMaxI->Value * 1000),
                   ROUND(feMaxP->Value * 10));
  }
}

//---------------------------- Save Preset: ---------------------------------

void __fastcall TSettingsForm::fePrv0Changed(TObject *Sender)
{
  if(!Lock)
  {
    TFloatEdit *fe = (TFloatEdit*)Sender;
    int n = fe->Tag;
    if(MainForm->FConnected)
      PSL_SetPre(n, ROUND(PreV[n]->Value * 100), ROUND(PreI[n]->Value * 1000));
  }
}

//--------------------------- Apply Preset: ---------------------------------

void __fastcall TSettingsForm::btPre0Click(TObject *Sender)
{
  TButton *bt = (TButton*)Sender;
  int n = bt->Tag;
  if(MainForm->FConnected)
    PSL_SetVI(ROUND(PreV[n]->Value * 100), ROUND(PreI[n]->Value * 1000), 0);
}

//--------------------------- Apply Calib: ----------------------------------

void __fastcall TSettingsForm::feCalVp1Changed(TObject *Sender)
{
  if(!Lock)
  {
    TFloatEdit *fe = (TFloatEdit*)Sender;
    int n = fe->Tag;
    int v = fe->Value;
    if(n == CAL_VP1 || n == CAL_VP2) v = ROUND(fe->Value * 100);
    if(n == CAL_IP1 || n == CAL_IP2) v = ROUND(fe->Value * 1000);
    if(MainForm->FConnected)
      PSL_SetCal(n, v);
  }
}

//---------------------------- Load Params: ---------------------------------

void __fastcall TSettingsForm::btReloadClick(TObject *Sender)
{
  TCursor Save_Cursor = Screen->Cursor;
  Lock = 1;
  if(MainForm->FConnected)
  {
    Screen->Cursor = crHourGlass;
    cbPAR_LOCK->Checked  = ReadPar(cbPAR_LOCK->Tag);
    cbPAR_TRC->Checked   = ReadPar(cbPAR_TRC->Tag);
    cbPAR_CON->Checked   = ReadPar(cbPAR_CON->Tag);
    cbPAR_ENR->Checked   = ReadPar(cbPAR_ENR->Tag);
    cbPAR_SET->Checked   = ReadPar(cbPAR_SET->Tag);
    cbPAR_GET->Checked   = ReadPar(cbPAR_GET->Tag);
    cbPAR_PRC->Checked   = ReadPar(cbPAR_PRC->Tag);
    cbPAR_SPL->Checked   = ReadPar(cbPAR_SPL->Tag);
    cbPAR_DNP->Checked   = ReadPar(cbPAR_DNP->Tag);
    cbPAR_OUT->Checked   = ReadPar(cbPAR_OUT->Tag);
    cbPAR_SND->ItemIndex = ReadPar(cbPAR_SND->Tag);
    cbPAR_APV->ItemIndex = ReadPar(cbPAR_APV->Tag);
    cbPAR_APC->ItemIndex = ReadPar(cbPAR_APC->Tag);
    fePAR_INF->Value     = ReadPar(fePAR_INF->Tag) / 100.0;
    fePAR_DEL->Value     = ReadPar(fePAR_DEL->Tag);
    fePAR_FAN->Value     = ReadPar(fePAR_FAN->Tag) / 10.0;
    fePAR_ALA->Value     = ReadPar(fePAR_ALA->Tag) / 10.0;
    int v, i, p;
    PSL_GetProt(v, i, p);
    feOVP->Value = v / 100.0;
    feOCP->Value = i / 1000.0;
    feOPP->Value = p / 10.0;
    PSL_GetVIPmax(v, i, p);
    feMaxV->Value = v / 100.0;
    feMaxI->Value = i / 1000.0;
    feMaxP->Value = p / 10.0;
    for(int n = 0; n < PRESETS; n++)
    {
      PSL_GetPre(n, v, i);
      PreV[n]->Value = v / 100.0;
      PreI[n]->Value = i / 1000.0;
    }
    double k;
    for(int n = 0; n < CALIBS; n++)
    {
      PSL_GetCal(n, v);
      k = v;
      if(n == CAL_VP1 || n == CAL_VP2) k = k / 100.0;
      if(n == CAL_IP1 || n == CAL_IP2) k = k / 1000.0;
      Calib[n]->Value = k;
    }
    Screen->Cursor = Save_Cursor;
  }
  Lock = 0;
}

//---------------------------------------------------------------------------

