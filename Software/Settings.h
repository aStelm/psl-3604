#ifndef SettingsH
#define SettingsH

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "FloatEdit.hpp"
#include <ComCtrls.hpp>

#define PRESETS 10
#define CALIBS  13

//---------------------------------------------------------------------------

class TSettingsForm : public TForm
{
__published:	// IDE-managed Components
  TButton *btCancel;
  TButton *btOk;
  TPageControl *PageControl1;
  TTabSheet *TabSheet1;
  TTabSheet *TabSheet2;
  TTabSheet *TabSheet4;
  TGroupBox *GroupBox2;
  TCheckBox *cbAutoConnect;
  TCheckBox *cbAutoLoad;
  TGroupBox *GroupBox1;
  TLabel *Label1;
  TCheckBox *cbAutoScroll;
  TComboBox *cbSample;
  TGroupBox *GroupBox3;
  TCheckBox *cbPAR_LOCK;
  TCheckBox *cbPAR_TRC;
  TCheckBox *cbPAR_CON;
  TGroupBox *GroupBox4;
  TCheckBox *cbPAR_SET;
  TCheckBox *cbPAR_GET;
  TCheckBox *cbPAR_PRC;
  TCheckBox *cbPAR_ENR;
  TCheckBox *cbPAR_SPL;
  TGroupBox *GroupBox6;
  TLabel *Label3;
  TComboBox *cbPAR_APV;
  TLabel *Label4;
  TComboBox *cbPAR_APC;
  TGroupBox *GroupBox7;
  TLabel *Label2;
  TComboBox *cbPAR_SND;
  TGroupBox *GroupBox5;
  TLabel *Label7;
  TLabel *Label8;
  TLabel *Label9;
  TLabel *Label10;
  TFloatEdit *fePAR_FAN;
  TFloatEdit *fePAR_ALA;
  TGroupBox *GroupBox8;
  TLabel *Label11;
  TFloatEdit *fePAR_INF;
  TGroupBox *GroupBox9;
  TCheckBox *cbPAR_DNP;
  TCheckBox *cbPAR_OUT;
  TGroupBox *GroupBox10;
  TLabel *Label12;
  TLabel *Label13;
  TLabel *Label14;
  TLabel *Label15;
  TLabel *Label16;
  TLabel *Label17;
  TFloatEdit *feOVP;
  TFloatEdit *feOCP;
  TFloatEdit *feOPP;
  TGroupBox *GroupBox11;
  TLabel *Label24;
  TLabel *Label25;
  TFloatEdit *fePAR_DEL;
  TGroupBox *GroupBox13;
  TLabel *Label5;
  TLabel *Label6;
  TLabel *Label26;
  TLabel *Label27;
  TLabel *Label28;
  TLabel *Label29;
  TFloatEdit *feMaxV;
  TFloatEdit *feMaxI;
  TFloatEdit *feMaxP;
  TTabSheet *TabSheet3;
  TGroupBox *GroupBox12;
  TFloatEdit *fePrv0;
  TFloatEdit *fePri0;
  TLabel *Label19;
  TLabel *Label20;
  TFloatEdit *fePrv1;
  TFloatEdit *fePri1;
  TLabel *Label22;
  TLabel *Label23;
  TFloatEdit *fePrv2;
  TFloatEdit *fePri2;
  TLabel *Label31;
  TLabel *Label32;
  TFloatEdit *fePrv3;
  TFloatEdit *fePri3;
  TLabel *Label34;
  TLabel *Label35;
  TFloatEdit *fePrv4;
  TFloatEdit *fePri4;
  TLabel *Label37;
  TLabel *Label38;
  TFloatEdit *fePrv5;
  TFloatEdit *fePri5;
  TLabel *Label40;
  TLabel *Label41;
  TFloatEdit *fePrv6;
  TFloatEdit *fePri6;
  TLabel *Label43;
  TLabel *Label44;
  TFloatEdit *fePrv7;
  TFloatEdit *fePri7;
  TLabel *Label46;
  TLabel *Label47;
  TFloatEdit *fePrv8;
  TFloatEdit *fePri8;
  TLabel *Label49;
  TLabel *Label50;
  TFloatEdit *fePrv9;
  TFloatEdit *fePri9;
  TLabel *Label52;
  TLabel *Label53;
  TButton *btReload;
  TButton *btPre0;
  TButton *btPre1;
  TButton *btPre2;
  TButton *btPre3;
  TButton *btPre4;
  TButton *btPre5;
  TButton *btPre6;
  TButton *btPre7;
  TButton *btPre8;
  TButton *btPre9;
  TTabSheet *TabSheet5;
  TGroupBox *GroupBox14;
  TLabel *Label18;
  TLabel *Label21;
  TLabel *Label30;
  TLabel *Label33;
  TFloatEdit *feCalVp1;
  TFloatEdit *feCalVc1;
  TFloatEdit *feCalVp2;
  TFloatEdit *feCalVc2;
  TLabel *Label64;
  TLabel *Label65;
  TGroupBox *GroupBox15;
  TLabel *Label36;
  TLabel *Label39;
  TLabel *Label42;
  TLabel *Label45;
  TLabel *Label48;
  TLabel *Label51;
  TFloatEdit *feCalIp1;
  TFloatEdit *feCalIc1;
  TFloatEdit *feCalIp2;
  TFloatEdit *feCalIc2;
  TGroupBox *GroupBox16;
  TLabel *Label55;
  TLabel *Label57;
  TFloatEdit *feCalVm1;
  TFloatEdit *feCalVm2;
  TGroupBox *GroupBox17;
  TLabel *Label54;
  TLabel *Label56;
  TFloatEdit *feCalIm1;
  TFloatEdit *feCalIm2;
  TFloatEdit *feCalDef;
  void __fastcall FormCreate(TObject *Sender);
  void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
  void __fastcall FormKeyPress(TObject *Sender, char &Key);
  void __fastcall btOkClick(TObject *Sender);
  void __fastcall btCancelClick(TObject *Sender);
  void __fastcall btDefaultClick(TObject *Sender);
  void __fastcall cbPAR_LOCKClick(TObject *Sender);
  void __fastcall cbPAR_SNDChange(TObject *Sender);
  void __fastcall feOVPChanged(TObject *Sender);
  void __fastcall fePAR_DELChanged(TObject *Sender);
  void __fastcall feMaxVChanged(TObject *Sender);
  void __fastcall btReloadClick(TObject *Sender);
  void __fastcall fePrv0Changed(TObject *Sender);
  void __fastcall btPre0Click(TObject *Sender);
  void __fastcall feCalVp1Changed(TObject *Sender);
private:	// User declarations

enum TopPars_t //��������� ��������:
{
  PAR_MAXV, //������������ ����������
  PAR_MAXI, //������������ ���
  PAR_MAXP, //������������ ��������
  PARS_TOP  
};


enum ProtData_t //��������� ������:
{
  PAR_OVP,  //�������� ���������� ������
  PAR_OCP,  //�������� ���� ������
  PAR_OPP,  //�������� �������� ������
  PAR_OTP,  //���������� �����������
  PARS_PROT
};

enum SetupData_t //��������� ���������:
{
  PAR_LOCK, //Lock Controls
  PAR_CALL, //Call Preset
  PAR_STOR, //Store Preset
  PAR_TRC,  //Track (OFF/ON)
  PAR_CON,  //Confirm (OFF/ON)
  PAR_SET,  //Indicate setpoint when regulated (OFF/ON)
  PAR_GET,  //Always indicate maesured values(OFF/ON)
  PAR_PRC,  //Current Preview (OFF/ON)
  PAR_DNP,  //Down Programmer (OFF/ON)
  PAR_OUT,  //Restore Out State (OFF/ON)
  PAR_SND,  //Sound (OFF/ALARM/ON)
  PAR_ENR,  //Encoder reverse (OFF/ON)
  PAR_SPL,  //Splash Screen (OFF/ON)
  PAR_APV,  //Average/peak V indication (AVERAGE/PEAK HIGH/PEAK LOW)
  PAR_APC,  //Average/peak I indication (AVERAGE/PEAK HIGH/PEAK LOW)
  PAR_DEL,  //OVP/OCP Delay
  PAR_FAN,  //Fan Start Temperature
  PAR_ALA,  //Alarm Temperature
  PAR_HST,  //Heatsink Measured Temperature
  PAR_INF,  //Firmware Version Info
  PAR_DEF,  //Set Defaults
  PAR_CAL,  //Calibration (NO/YES/DEFAULT)
  PAR_ESC,  //����� �� ���� (NO/YES)
  PARS_SETUP  
};

enum CalibData_t //��������� ����������:
{
  CAL_VP1,  //���������� V, ����� 1
  CAL_VC1,  //���������� V, ��� 1
  CAL_VP2,  //���������� V, ����� 2
  CAL_VC2,  //���������� V, ��� 2
  CAL_IP1,  //���������� I, ����� 1
  CAL_IC1,  //���������� I, ��� 1
  CAL_IP2,  //���������� I, ����� 2
  CAL_IC2,  //���������� I, ��� 2
  CAL_STR,  //���������� ����������
  CAL_VM1,  //���������� MeterV, ��� 1
  CAL_VM2,  //���������� MeterV, ��� 2
  CAL_IM1,  //���������� MeterI, ��� 1
  CAL_IM2,  //���������� MeterI, ��� 2
  CAL_CNT
};

  int __fastcall ReadPar(int n);
  TFloatEdit *PreV[PRESETS];
  TFloatEdit *PreI[PRESETS];
  TFloatEdit *Calib[CALIBS];
  bool Lock;
public:		// User declarations
  __fastcall TSettingsForm(TComponent* Owner);
};

//---------------------------------------------------------------------------

extern PACKAGE TSettingsForm *SettingsForm;

//---------------------------------------------------------------------------

#endif
