object ConnectForm: TConnectForm
  Left = 544
  Top = 451
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Connect...'
  ClientHeight = 229
  ClientWidth = 217
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object btCancel: TButton
    Left = 135
    Top = 196
    Width = 75
    Height = 25
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 1
    OnClick = btCancelClick
  end
  object btOk: TButton
    Left = 55
    Top = 196
    Width = 75
    Height = 25
    Caption = '&OK'
    ModalResult = 1
    TabOrder = 0
    OnClick = btOkClick
  end
  object DevListView: TListView
    Left = 8
    Top = 8
    Width = 201
    Height = 177
    Columns = <
      item
        Caption = 'Port'
        Width = 60
      end
      item
        Caption = 'Device name'
        Width = 120
      end>
    ColumnClick = False
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    TabOrder = 2
    ViewStyle = vsReport
    OnDblClick = btOkClick
  end
end
