unit FloatEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
	TFloat = double;

	TValidateValueEvent = procedure (Sender: TObject; Value: TFloat;
		var Valid: boolean) of object;
	TErrorEvent = procedure (Sender: TObject; const Msg: string;
		var Handled: boolean) of object;

	TFloatEdit = class(TCustomEdit)
	private
		FMinMaxCheck: boolean;
		FPrec: byte;
		FDigits: byte;
		FOnChanged: TNotifyEvent;
		FOnError: TErrorEvent;
		FMinValue: TFloat;
		FValue: TFloat;
		FMaxValue: TFloat;
		FFormat: TFloatFormat;
		FValueVisible: boolean;
		FOnValidateValue: TValidateValueEvent;
		FOnOutOfBounds: TNotifyEvent;
		FOnExit: TNotifyEvent;
		FOnEnter: TNotifyEvent;
		FOnEnterPress: TNotifyEvent;
		procedure SetDigits(const Value: byte);
		procedure SetFormat(const Value: TFloatFormat);
		procedure SetMaxValue(const Value: TFloat);
		procedure SetMinValue(const Value: TFloat);
		procedure SetPrec(const Value: byte);
		procedure SetValue(const aValue: TFloat);
        procedure SetValueAnyway(const aValue: TFloat);
		procedure SetValueVisible(const aValue: boolean);

		//�������� �������� Text � ������������ � Value
		procedure UpdateText;

		//��������� ������������ �������� Value
		function IsValidValue(Value: TFloat): boolean;
		//��������� ������������ �������� �������� Value �,
		//���� ��������, ������������� ��� � ���������� ��������
		procedure CheckValue;

		procedure MyOnExit(Sender: TObject);	//������ ������ OnExit
		procedure MyOnEnter(Sender: TObject);	//������ ������ OnEnter
	protected
		procedure KeyDown(var Key: Word; Shift: TShiftState); override;
	public
		constructor Create(AOwner: TComponent); override;
		destructor Destroy; override;

		procedure SetValueStr(const aValue: string);
	published
		//������ �������������� Value � Text
		property Format: TFloatFormat read FFormat write SetFormat;
		property Digits: byte read FDigits write SetDigits;
		property Precision: byte read FPrec write SetPrec;

		//����������, ����� �� ��� �������� ������������ �������� Value
		//�������������� �������� MinValue & MaxValue (MinMaxCheck = true)
		//��� ���������� ������� OnValidateValue (MinMaxCheck = false)
		property MinMaxCheck: boolean read FMinMaxCheck write FMinMaxCheck;

		//���������� ������� �������� Value
		property MinValue: TFloat read FMinValue write SetMinValue;
		property MaxValue: TFloat read FMaxValue write SetMaxValue;

		//������� ��������
		property Value: TFloat read FValue write SetValue;
		//������������ �� ���������� ����������
		property ValueVisible: boolean read FValueVisible write SetValueVisible;
{    //������� �������� � ���� ������
		property Text: string read GetText;
}
		//���������, ����� ������������ ��� ���������� �������� � ���������� ���
		property OnChanged: TNotifyEvent read FOnChanged write FOnChanged;
		//���������, ����� ����� ��������� ������������ �������� Value � MinMaxCheck = false;
		property OnValidateValue: TValidateValueEvent read FOnValidateValue
			write FOnValidateValue;
		//���������, ���� �������� Value ����� �� ������������� �����
		property OnOutOfBounds: TNotifyEvent read FOnOutOfBounds write FOnOutOfBounds;
		//��������� � ������ ������ ��� �������������� Text -> Value
		property OnError: TErrorEvent read FOnError write FOnError;
		//��������� ����� ������� OnChanged, ���� ��� ���� ������� �������� Enter
		property OnEnterPress: TNotifyEvent read FOnEnterPress write FOnEnterPress;
		//��������� ��� ������ ������������� ������
		property OnExit: TNotifyEvent read FOnExit write FOnExit;
		//��������� ��� ��������� ������������� ������
		property OnEnter: TNotifyEvent read FOnEnter write FOnEnter;

		property Anchors;
		property AutoSelect;
		property AutoSize;
		property BiDiMode;
		property BorderStyle;
//		property CharCase;
		property Color;
		property Constraints;
		property Ctl3D;
		property DragCursor;
		property DragKind;
		property DragMode;
		property Enabled;
		property Font;
		property HideSelection;
		property ImeMode;
		property ImeName;
//		property MaxLength;
//		property OEMConvert;
		property ParentBiDiMode;
		property ParentColor;
		property ParentCtl3D;
		property ParentFont;
		property ParentShowHint;
//		property PasswordChar;
		property PopupMenu;
		property ReadOnly;
		property ShowHint;
		property TabOrder;
		property TabStop;
//		property Text;
		property Visible;
//		property OnChange;
		property OnClick;
		property OnContextPopup;
		property OnDblClick;
		property OnDragDrop;
		property OnDragOver;
		property OnEndDock;
		property OnEndDrag;
//		property OnEnter;
//		property OnExit;
		property OnKeyDown;
		property OnKeyPress;
		property OnKeyUp;
		property OnMouseDown;
		property OnMouseMove;
		property OnMouseUp;
		property OnStartDock;
		property OnStartDrag;
	end;

procedure Register;

implementation

procedure Register;
begin
	RegisterComponents('Sp', [TFloatEdit]);
end;

{ TFloatEdit }

constructor TFloatEdit.Create(AOwner: TComponent);
begin
	inherited;
	FFormat := ffFixed;
	FPrec := 5;
	FDigits := 5;
	FMinMaxCheck := true;
	FMinValue := 0.0;
	FMaxValue := 10.0;
	FValue := 0.0;
	FValueVisible := true;
    AutoSize := false;
	UpdateText;
	inherited OnExit := MyOnExit;
	inherited OnEnter := MyOnEnter;	
end;

destructor TFloatEdit.Destroy;
begin
	inherited;
end;

function TFloatEdit.IsValidValue(Value: TFloat): boolean;
begin
	if FMinMaxCheck then
		Result := (Value >= MinValue) and (Value <=MaxValue)
	else
		begin
			Result := true;
			if Assigned(FOnValidateValue) then
				FOnValidateValue(Self, Value, Result);
		end;
end;

procedure TFloatEdit.CheckValue;

	procedure SetToValid;
	begin
		if (FValue > FMaxValue) then
			FValue := FMaxValue
		else
			FValue := FMinValue;
		if assigned(FOnChanged) then
            FOnChanged(Self);
	end;//SetToValid

begin
	if (not IsValidValue(FValue)) then
		begin
			if Assigned(FOnOutOfBounds) then
				FOnOutOfBounds(Self)
			else
				if (FMinMaxCheck) then
					SetToValid;
//			UpdateText;
		end;
end;

procedure TFloatEdit.SetDigits(const Value: byte);
begin
	FDigits := Value;
	UpdateText;
end;

procedure TFloatEdit.SetFormat(const Value: TFloatFormat);
begin
	FFormat := Value;
	UpdateText;
end;

procedure TFloatEdit.SetMaxValue(const Value: TFloat);
begin
	FMaxValue := Value;
	CheckValue;
    UpdateText;
end;

procedure TFloatEdit.SetMinValue(const Value: TFloat);
begin
	FMinValue := Value;
	CheckValue;
    UpdateText;
end;

procedure TFloatEdit.SetPrec(const Value: byte);
begin
	FPrec := Value;
	UpdateText;
end;

procedure TFloatEdit.SetValue(const aValue: TFloat);
begin
    if FValue<>aValue then
        SetValueAnyway(aValue);
end;

//��������� ��������, ���� ���� ��� ����� ��������
procedure TFloatEdit.SetValueAnyway(const aValue: TFloat);
begin
    FValue := aValue;
    CheckValue;
    if Assigned(FOnChanged) then
        FOnChanged(Self);
    UpdateText;
    Repaint;
    SelectAll;
end;

procedure TFloatEdit.UpdateText;
var
    s: string;
begin
	if FValueVisible then
        begin
    		s := FloatToStrF(FValue, FFormat, FPrec, FDigits);
            SetTextBuf(PChar(s));
        end
	else
		SetTextBuf(nil);
	Invalidate;
end;

procedure TFloatEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
	inherited;
	if (Key = VK_RETURN) then
		begin
			SetValueStr(Text);
			if assigned(FOnEnterPress) then
				FOnEnterPress(Self);
		end;
end;

procedure TFloatEdit.SetValueStr(const aValue: string);
var
    Handled: boolean;
    curVal: string;
    curFVal: TFloat;
begin
        curVal := FloatToStrF(FValue, FFormat, FPrec, FDigits);
        curFVal := FValue;
        if (aValue<>curVal) then
         begin
          try
           curFVal := StrToFloat(aValue);
          except
	   on e1: EConvertError do
            begin
	     Handled := false;
	     if assigned(FOnError) then
	      FOnError(Self, e1.Message, Handled);
	      if (not Handled) then curFVal := FValue;
            end;
          end;
         end;
    	SetValueAnyway(curFVal);
end;

procedure TFloatEdit.SetValueVisible(const aValue: boolean);
begin
	FValueVisible := aValue;
	UpdateText;
end;

procedure TFloatEdit.MyOnExit(Sender: TObject);
begin
//	UpdateText;//�� ��������� ��������� ��������� ������
	SetValueStr(Text); //������������. ������ ��������� ������
	UpdateText;
	if Assigned(FOnExit) then
		FOnExit(Sender);
end;

procedure TFloatEdit.MyOnEnter(Sender: TObject);
begin
	SelectAll;
	if Assigned(FOnEnter) then
		FOnEnter(Sender);
end;

end.
