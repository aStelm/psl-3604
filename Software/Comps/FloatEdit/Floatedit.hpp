// Borland C++ Builder
// Copyright (c) 1995, 2002 by Borland Software Corporation
// All rights reserved

// (DO NOT EDIT: machine generated header) 'FloatEdit.pas' rev: 6.00

#ifndef FloatEditHPP
#define FloatEditHPP

#pragma delphiheader begin
#pragma option push -w-
#pragma option push -Vx
#include <Menus.hpp>	// Pascal unit
#include <StdCtrls.hpp>	// Pascal unit
#include <Dialogs.hpp>	// Pascal unit
#include <Forms.hpp>	// Pascal unit
#include <Controls.hpp>	// Pascal unit
#include <Graphics.hpp>	// Pascal unit
#include <Classes.hpp>	// Pascal unit
#include <SysUtils.hpp>	// Pascal unit
#include <Messages.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <System.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Floatedit
{
//-- type declarations -------------------------------------------------------
typedef double TFloat;

typedef void __fastcall (__closure *TValidateValueEvent)(System::TObject* Sender, double Value, bool &Valid);

typedef void __fastcall (__closure *TErrorEvent)(System::TObject* Sender, const AnsiString Msg, bool &Handled);

class DELPHICLASS TFloatEdit;
class PASCALIMPLEMENTATION TFloatEdit : public Stdctrls::TCustomEdit 
{
	typedef Stdctrls::TCustomEdit inherited;
	
private:
	bool FMinMaxCheck;
	Byte FPrec;
	Byte FDigits;
	Classes::TNotifyEvent FOnChanged;
	TErrorEvent FOnError;
	double FMinValue;
	double FValue;
	double FMaxValue;
	Sysutils::TFloatFormat FFormat;
	bool FValueVisible;
	TValidateValueEvent FOnValidateValue;
	Classes::TNotifyEvent FOnOutOfBounds;
	Classes::TNotifyEvent FOnExit;
	Classes::TNotifyEvent FOnEnter;
	Classes::TNotifyEvent FOnEnterPress;
	void __fastcall SetDigits(const Byte Value);
	void __fastcall SetFormat(const Sysutils::TFloatFormat Value);
	void __fastcall SetMaxValue(const double Value);
	void __fastcall SetMinValue(const double Value);
	void __fastcall SetPrec(const Byte Value);
	void __fastcall SetValue(const double aValue);
	void __fastcall SetValueAnyway(const double aValue);
	void __fastcall SetValueVisible(const bool aValue);
	void __fastcall UpdateText(void);
	bool __fastcall IsValidValue(double Value);
	void __fastcall CheckValue(void);
	void __fastcall MyOnExit(System::TObject* Sender);
	void __fastcall MyOnEnter(System::TObject* Sender);
	
protected:
	DYNAMIC void __fastcall KeyDown(Word &Key, Classes::TShiftState Shift);
	
public:
	__fastcall virtual TFloatEdit(Classes::TComponent* AOwner);
	__fastcall virtual ~TFloatEdit(void);
	void __fastcall SetValueStr(const AnsiString aValue);
	
__published:
	__property Sysutils::TFloatFormat Format = {read=FFormat, write=SetFormat, nodefault};
	__property Byte Digits = {read=FDigits, write=SetDigits, nodefault};
	__property Byte Precision = {read=FPrec, write=SetPrec, nodefault};
	__property bool MinMaxCheck = {read=FMinMaxCheck, write=FMinMaxCheck, nodefault};
	__property double MinValue = {read=FMinValue, write=SetMinValue};
	__property double MaxValue = {read=FMaxValue, write=SetMaxValue};
	__property double Value = {read=FValue, write=SetValue};
	__property bool ValueVisible = {read=FValueVisible, write=SetValueVisible, nodefault};
	__property Classes::TNotifyEvent OnChanged = {read=FOnChanged, write=FOnChanged};
	__property TValidateValueEvent OnValidateValue = {read=FOnValidateValue, write=FOnValidateValue};
	__property Classes::TNotifyEvent OnOutOfBounds = {read=FOnOutOfBounds, write=FOnOutOfBounds};
	__property TErrorEvent OnError = {read=FOnError, write=FOnError};
	__property Classes::TNotifyEvent OnEnterPress = {read=FOnEnterPress, write=FOnEnterPress};
	__property Classes::TNotifyEvent OnExit = {read=FOnExit, write=FOnExit};
	__property Classes::TNotifyEvent OnEnter = {read=FOnEnter, write=FOnEnter};
	__property Anchors  = {default=3};
	__property AutoSelect  = {default=1};
	__property AutoSize  = {default=1};
	__property BiDiMode ;
	__property BorderStyle  = {default=1};
	__property Color  = {default=-2147483643};
	__property Constraints ;
	__property Ctl3D ;
	__property DragCursor  = {default=-12};
	__property DragKind  = {default=0};
	__property DragMode  = {default=0};
	__property Enabled  = {default=1};
	__property Font ;
	__property HideSelection  = {default=1};
	__property ImeMode  = {default=3};
	__property ImeName ;
	__property ParentBiDiMode  = {default=1};
	__property ParentColor  = {default=0};
	__property ParentCtl3D  = {default=1};
	__property ParentFont  = {default=1};
	__property ParentShowHint  = {default=1};
	__property PopupMenu ;
	__property ReadOnly  = {default=0};
	__property ShowHint ;
	__property TabOrder  = {default=-1};
	__property TabStop  = {default=1};
	__property Visible  = {default=1};
	__property OnClick ;
	__property OnContextPopup ;
	__property OnDblClick ;
	__property OnDragDrop ;
	__property OnDragOver ;
	__property OnEndDock ;
	__property OnEndDrag ;
	__property OnKeyDown ;
	__property OnKeyPress ;
	__property OnKeyUp ;
	__property OnMouseDown ;
	__property OnMouseMove ;
	__property OnMouseUp ;
	__property OnStartDock ;
	__property OnStartDrag ;
public:
	#pragma option push -w-inl
	/* TWinControl.CreateParented */ inline __fastcall TFloatEdit(HWND ParentWindow) : Stdctrls::TCustomEdit(ParentWindow) { }
	#pragma option pop
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE void __fastcall Register(void);

}	/* namespace Floatedit */
using namespace Floatedit;
#pragma option pop	// -w-
#pragma option pop	// -Vx

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// FloatEdit
