//----------------------------------------------------------------------------

//������ ��������� ������� EEPROM, ������������ ����

//----------------------------------------------------------------------------

#ifndef EEPROM_H
#define EEPROM_H

//----------------------------------------------------------------------------

#include "timer.h"

//----------------------------- ���������: -----------------------------------

//TODO: ������� �������� ������ �� ������� ������ EEPROM.
#define EEPROM_SIZE 512 //����� ���������� ������ 24�04, ����

//----------------------------------------------------------------------------
//---------------------------- ����� TEeprom: --------------------------------
//----------------------------------------------------------------------------

class TEeprom
{
private:
  static TGpio<PORTB, PIN8> Pin_SCL; 
  static TGpio<PORTB, PIN9> Pin_SDA;
  static void I2C_Start(void);
  static bool I2C_Write(char data);
  static char I2C_Read(bool ack);
  static void I2C_Stop(void);
  static void I2C_Free(void);
  static void I2C_Delay(void);
  static TSoftTimer *WriteTimer;
protected:
  static uint16_t EeTop;
  static uint16_t Read(uint16_t addr);
  static void Write(uint16_t addr, uint16_t data);
  static void Update(uint16_t addr, uint16_t data);
public:
  static void Init(void);
  static bool Error;
};

//----------------------------------------------------------------------------
//--------------------------- ����� TEeSection: ------------------------------
//----------------------------------------------------------------------------

class TEeSection : public TEeprom
{
private:
protected:
  uint16_t Base;
  uint16_t Size;
  uint16_t Sign;
public:
  TEeSection(uint16_t size);
  bool Valid;
  virtual void Validate(void);
  uint16_t Read(uint16_t addr);
  void Write(uint16_t addr, uint16_t data);
  void Update(uint16_t addr, uint16_t data);
};

//----------------------------------------------------------------------------
//--------------------------- ����� TCrcSection: -----------------------------
//----------------------------------------------------------------------------

class TCrcSection : public TEeSection
{
private:
  uint16_t Crc;
  uint16_t GetCRC(void);
protected:
public:
  TCrcSection(uint16_t size);
  virtual void Validate(void);
};

//----------------------------------------------------------------------------
//--------------------------- ����� TRingSection: ----------------------------
//----------------------------------------------------------------------------

class TRingSection : public TEeSection
{
private:
  uint16_t Ptr;
protected:
public:
  TRingSection(uint16_t size);
  //TODO: �� ����� ������, ��� ����� ����������� ��������������
  //�������. ���� � ������������ ������ ������� � ����� �������
  //������� ������������, �� ��� ��������������� ����� �������������
  //��� ���������, �.�. ������������ ������� � ������ ����������
  //���� ����� �� �����. ����� ��� � �����, �� ���������� ������ warning.
  uint16_t Read(void);
  void Write(uint16_t data);
  void Update(uint16_t data);
};

//----------------------------------------------------------------------------

#endif
