//----------------------------------------------------------------------------

//������ ��������� LED-�������, ������������ ����

//----------------------------------------------------------------------------

#ifndef DISPLAY_H
#define DISPLAY_H

//----------------------------------------------------------------------------

#include "timer.h"
#include "sreg.h"

//----------------------------- ���������: -----------------------------------

#define POINT      0x80 //���� ����������� �����
#define AUTO_SCALE 0x80 //���� �������������������

enum PosName_t //����� ������� �������
{
  POS_1,
  POS_2,
  POS_3,
  POS_4,
  DIGS
};

enum RowName_t //����� ����� �������
{
  ROW_V,
  ROW_I,
  ROWS
};

enum Blink_t //����� ������� �������
{
  BLINK_NO   = 0x00,
  BLINK_V    = 0x01,
  BLINK_I    = 0x02,
  BLINK_VI   = 0x03
};

//----------------------------------------------------------------------------
//---------------------------- ����� TDisplay: -------------------------------
//----------------------------------------------------------------------------

class TDisplay
{
private:
  char SegDataV[DIGS];
  char SegDataI[DIGS];
  char Row;
  char Pos;
  char Conv(char d);
  char SetScan(char phase);
  Blink_t BlinkEn;
  bool BlinkOn;
  bool DispOn;
  TSreg Sreg;
  TSoftTimer *BlinkTimer;
public:
  TDisplay(void);
  void Execute(void); 
  bool LedCV;              //���������� ����������� CV
  bool LedCC;              //���������� ����������� CC
  bool LedOut;             //���������� ����������� OUT
  void Blink(Blink_t blink); //���������/���������� �������
  bool LedFine;            //���������� ����������� FINE
  void Clear(void);        //������� �������
  void Off(void);          //���������� ������� (������ �����)
  void On(void);           //��������� ������� (������ �����)
  void Disable(void);      //���������� ���� ���������
  void Enable(void);       //��������� ���� ���������
  void SetPos(char row, char pos); //��������� �������
  void PutChar(char ch);   //����� �������
  void PutString(char *s); //����� ������ �� RAM
  void PutString(const char *s); //����� ������ �� ROM
  void PutIntF(int32_t v, char n, char d); //��������������� ����� �����
};

//----------------------------------------------------------------------------

extern TDisplay *Display;

//----------------------------------------------------------------------------

#endif
