//----------------------------------------------------------------------------

//������ ��������� ������� EEPROM

//----------------------- ������������ �������: ------------------------------

//������������ ������� ���������� EEPROM ���� 24�04, ������� ����������
//� ����� SCL (PB8), SDA (PB9). ��������� I2C ����������� ����������.
//� ���������, ���������� I2C1 (remap) ������������ ������,
//��� ��� �� ����������� � SPI1 (remap): �� ������ PB5
//������ ������� (��. errata).
//��� ������������ ��������� ���� ������������ ������ TIM16.

//----------------------------------------------------------------------------

#include "main.h"
#include "eeprom.h"

//----------------------------- ���������: -----------------------------------

#define I2C_CLK    100 //������� ���� I2C, ��� TODO: ������� 400?
#define I2C_RD       1 //������� ������ ������ �� ���� I2C
#define I2C_ADDR  0xA0 //����� ���������� EEPROM
#define I2C_ACK      1 //������� �������� ACK
#define I2C_NACK     0 //������� �������� NACK
#define EEPROM_WRTM  5 //������������ ����� ������, ��

#define EE_SIGNATURE 0xBEDA

#define I2C_DELAY (APB2_CLOCK / 1000 / I2C_CLK / 2 - 18)

//----------------------------------------------------------------------------
//----------------------------- ����� TEEPROM: -------------------------------
//----------------------------------------------------------------------------

//----------------------------- �������������: -------------------------------

uint16_t TEeprom::EeTop;
bool TEeprom::Error;
TSoftTimer* TEeprom::WriteTimer;
TGpio<PORTB, PIN8> TEeprom::Pin_SCL; 
TGpio<PORTB, PIN9> TEeprom::Pin_SDA;

void TEeprom::Init(void)
{
  Pin_SCL.Init(OUT_OD_2M, OUT_HI);
  Pin_SDA.Init(OUT_OD_2M, OUT_HI);

  RCC->APB2ENR |= RCC_APB2ENR_TIM16EN; //��������� ������������ TIM16
  TIM16->PSC = 0;                      //�������� ����������
  TIM16->ARR = 0xFFFF;                 //������
  TIM16->CR1 = TIM_CR1_CEN;            //���������� �������
  
  WriteTimer = new TSoftTimer(EEPROM_WRTM);
  WriteTimer->Force();
  
  I2C_Free(); //����� I2C
  I2C_Stop();
  EeTop = 0;  //������ ��������� �������
  Error = 0;
}

//----------------------------------------------------------------------------
//---------------------------- ������ � I2C: ---------------------------------
//----------------------------------------------------------------------------

//------------------- ��������� ������� "�����" �� I2C: ----------------------

void TEeprom::I2C_Start(void)
{
  Pin_SDA = 0;
  I2C_Delay();
}

//------------------------- ������ ����� �� I2C: -----------------------------

bool TEeprom::I2C_Write(char data)
{
  bool ack = 0;
  for(char i = 0; i < 8; i++)
  {
    Pin_SCL = 0;
    if(data & 0x80)
      Pin_SDA = 1;
        else Pin_SDA = 0;
    data <<= 1;    
    I2C_Delay();
    Pin_SCL = 1;
    I2C_Delay();
  }
  Pin_SCL = 0;
  Pin_SDA = 1;
  I2C_Delay();
  Pin_SCL = 1;
  I2C_Delay();
  ack = !Pin_SDA;
  I2C_Delay();
  return(ack);
}

//------------------------- ������ ����� �� I2C: -----------------------------

char TEeprom::I2C_Read(bool ack)
{
  char data;
  for(char i = 0; i < 8; i++)
  {
    Pin_SCL = 0;
    Pin_SDA = 1;
    I2C_Delay();
    Pin_SCL = 1;
    I2C_Delay();
    data = data << 1;
    if(Pin_SDA) data |= 0x01;
  }
  Pin_SCL = 0;
  Pin_SDA = !ack;
  I2C_Delay();
  Pin_SCL = 1;
  I2C_Delay();
  return(data);
}

//------------------- ��������� ������� "����" �� I2C: -----------------------

void TEeprom::I2C_Stop(void)
{
  Pin_SCL = 0;
  Pin_SDA = 0;
  I2C_Delay();
  Pin_SCL = 1;
  I2C_Delay();
  Pin_SDA = 1;
  I2C_Delay();
}

//------------------------ ������������ ���� I2C: ----------------------------

void TEeprom::I2C_Free(void)
{
  for(char i = 0; i < 9; i++)
  {
    if(Pin_SDA) break;
    Pin_SCL = 0;
    I2C_Delay();
    Pin_SCL = 1;
    I2C_Delay();
  }
}

//---------------------- �������� 1/2 ������� I2C: ---------------------------

void TEeprom::I2C_Delay(void)
{
  TIM16->CNT = 0;
  while(TIM16->CNT < I2C_DELAY);
}

//----------------------------------------------------------------------------
//-------------------------- ������ � EEPROM: --------------------------------
//----------------------------------------------------------------------------

//---------------------- ������ ������ �� EEPROM: ----------------------------

//addr - ����� �����
//data - ����� ������ ��� ������ � EEPROM

uint16_t TEeprom::Read(uint16_t addr)
{
  while(!WriteTimer->Over());
  char byte_address = (addr << 1) & 0xFE;
  char page_address = (addr >> 6) & 0x0E;
  I2C_Start();
  I2C_Write(I2C_ADDR | page_address);
  I2C_Write(byte_address);
  I2C_Stop();
  I2C_Start();
  I2C_Write(I2C_ADDR | page_address | I2C_RD);
  char data_l = I2C_Read(I2C_ACK);
  char data_h = I2C_Read(I2C_NACK);
  return(WORD(data_h, data_l));
}

//----------------------- ������ ������ � EEPROM: ----------------------------

//addr - ����� �����
//data - ����� ������ ��� ������ � EEPROM

void TEeprom::Write(uint16_t addr, uint16_t data)
{
  while(!WriteTimer->Over());
  char byte_address = (addr << 1) & 0xFE;
  char page_address = (addr >> 6) & 0x0E;
  I2C_Start();
  I2C_Write(I2C_ADDR | page_address);
  I2C_Write(byte_address);
  I2C_Write(LO(data));
  I2C_Write(HI(data));
  I2C_Stop();
  WriteTimer->Start();
}

//--------------------- ���������� ������ � EEPROM: --------------------------

//addr - ����� �����
//data - ����� ������ ��� ������ � EEPROM
//������ ������������ ������ � ��� ������, ���� ����� ������ ����������.

void TEeprom::Update(uint16_t addr, uint16_t data)
{
  uint16_t d = Read(addr);
  if(data != d)
    Write(addr, data);
}

//----------------------------------------------------------------------------
//--------------------------- ����� TEeSection: ------------------------------
//----------------------------------------------------------------------------

//������� ������ EEPROM, ���������� ������
//��������������� ��������� ���������.

//----------------------------- �����������: ---------------------------------

TEeSection::TEeSection(uint16_t size)
{
  Base = EeTop;       //������ ������
  Size = size;        //������ ������
  Sign = Base + size; //�������� ���������
  EeTop = Sign + 1;   //����� ������ ���������� ����� EEPROM
  Valid = TEeprom::Read(Sign) == EE_SIGNATURE;
  if(!Valid) TEeprom::Error = 1;
}

//------------------------- ��������� ����������: ----------------------------

void TEeSection::Validate(void)
{
  TEeprom::Update(Sign, EE_SIGNATURE);
  Valid = 1;
}

//------------------------- ������ ������ ������: ----------------------------

uint16_t TEeSection::Read(uint16_t addr)
{
  return(TEeprom::Read(Base + addr));
}

//------------------------- ������ ������ ������: ----------------------------

void TEeSection::Write(uint16_t addr, uint16_t data)
{
  if(addr < Size)
    TEeprom::Write(Base + addr, data);
}

//----------------------- ���������� ������ ������: --------------------------

void TEeSection::Update(uint16_t addr, uint16_t data)
{
  if(addr < Size)
    TEeprom::Update(Base + addr, data);
}

//----------------------------------------------------------------------------
//--------------------------- ����� TCrcSection: -----------------------------
//----------------------------------------------------------------------------

//������ EEPROM ���������� ����������, ���������� ������
//��������������� ��������� ��������� � CRC.

//----------------------------- �����������: ---------------------------------

TCrcSection::TCrcSection(uint16_t size) : TEeSection(size)
{
  Crc = EeTop;     //�������� CRC
  EeTop = Crc + 1; //����� ������ ���������� ����� EEPROM
  Valid = Valid && (TEeprom::Read(Crc) == GetCRC());
  if(!Valid) TEeprom::Error = 1;
}

//------------------------------ ������ CRC: ---------------------------------

uint16_t TCrcSection::GetCRC(void)
{
  RCC->AHBENR |= RCC_AHBENR_CRCEN;
  CRC->CR = CRC_CR_RESET;
  for(uint16_t i = 0; i < Size; i++)
    CRC->DR = (uint32_t)TEeprom::Read(Base + i);
  uint16_t result = CRC->DR;
  RCC->AHBENR &= ~RCC_AHBENR_CRCEN;
  return(result);
}

//------------------------- ��������� ����������: ----------------------------

void TCrcSection::Validate(void)
{
  TEeSection::Validate();
  TEeprom::Update(Crc, GetCRC());
  Valid = 1;
}

//----------------------------------------------------------------------------
//--------------------------- ����� TRingSection: ----------------------------
//----------------------------------------------------------------------------

//������ EEPROM ����������� �������, ���� ����� ������ ��������� ���
//�������� ����� ����������. ������������ ��������� �����, �����
//�������� ������������ �� ���������� ������, ���������� ��������
//��������� (������������ 0xFFFF). ���������� ������ ���������������
//��������� ���������.

//----------------------------- �����������: ---------------------------------

TRingSection::TRingSection(uint16_t size) : TEeSection(size)
{
  Ptr = 0;
  //����� ������:
  if(Valid)
    while((Ptr < Size) && (TEeprom::Read(Base + Ptr) == 0xFFFF))
      Ptr++;
  if(Ptr == Size) Ptr = 0;
}

//----------------------- - ������ ������ ������: ----------------------------

uint16_t TRingSection::Read(void)
{
  return(TEeprom::Read(Base + Ptr));
}

//--------------------- --- ������ ������ ������: ----------------------------

void TRingSection::Write(uint16_t data)
{
  uint16_t pre = Ptr;
  if(++Ptr == Size) Ptr = 0;
  TEeprom::Write(Base + Ptr, data);
  TEeprom::Write(Base + pre, 0xFFFF);
}

//----------------- ----- ���������� ������ ������: --------------------------

void TRingSection::Update(uint16_t data)
{
  if(Read() != data)
    Write(data);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
