//----------------------------------------------------------------------------

//������ ������, ������������ ����

//----------------------------------------------------------------------------

#ifndef DATA_H
#define DATA_H

//----------------------------------------------------------------------------

#include "display.h"
#include "eeprom.h"

//------------------------------- ���������: ---------------------------------

#define PRESETS 10 //���������� ��������
#define RING_V 160 //������ ���������� ������ V

#define DMX    999 //����. ���������� �������� ��������, ��

#define TMX    999 //����. ���������� �������� �����������, �0.1�C
#define TFN    500 //����������� ����������� ���. �����������, �0.1�C
#define TAL    600 //����������� ����������� ������, �0.1�C
#define T_OVER  30 //���������� ����������� ��� ����������, �0.1�C

//��������� ��������:

enum TopPars_t
{
  PAR_MAXV, //������������ ����������
  PAR_MAXI, //������������ ���
  PAR_MAXP, //������������ ��������
  PARS_TOP  
};

//TODO: �������� ������������ �������� ���� �� ������������.
//�������������� ������������ �� ��� ���������� PowerFlex.
//�� �� ������ �������, ���. ���� ������ ��� ������� ����������
//������������ ���������� ���, �� ��� ������ CC �������� �����
//���������� �� ������� ������ ������. ������ ����������� ����
//�������� ����������� ����������? ��� ������������� ��� ��������
//��� ���������� ���������� ��������� ������� OVP, OCP? �����
//�������� ������ ������������ ����� OPP, ��� �������� PAR_MAXP
//����� ������� ���������� ���������.

//�������� ���������:

enum MainPars_t
{
  PAR_V,    //������������� ����������
  PAR_I,    //������������� ���
  PAR_FINE, //��������� FINE (OFF/ON)
  PARS_MAIN  
};

//��������� ������:

enum ProtData_t
{
  PAR_OVP,  //�������� ���������� ������
  PAR_OCP,  //�������� ���� ������
  PAR_OPP,  //�������� �������� ������
  PAR_OTP,  //���������� �����������
  PARS_PROT
};

#define PROT_FLAG 0x80 //���� ������������ ������

//��������� ���������:

enum SetupData_t
{
  PAR_LOCK, //Lock Controls
  PAR_CALL, //Call Preset
  PAR_STOR, //Store Preset
  PAR_TRC,  //Track (OFF/ON)
  PAR_CON,  //Confirm (OFF/ON)
  PAR_SET,  //Indicate setpoint when regulated (OFF/ON)
  PAR_GET,  //Always indicate maesured values(OFF/ON)
  PAR_PRC,  //Current Preview (OFF/ON)
  PAR_DNP,  //Down Programmer (OFF/ON)
  PAR_OUT,  //Restore Out State (OFF/ON)
  PAR_SND,  //Sound (OFF/ALARM/ON)
  PAR_ENR,  //Encoder reverse (OFF/ON)
  PAR_SPL,  //Splash Screen (OFF/ON)
  PAR_APV,  //Average/peak V indication (AVERAGE/PEAK HIGH/PEAK LOW)
  PAR_APC,  //Average/peak I indication (AVERAGE/PEAK HIGH/PEAK LOW)
  PAR_DEL,  //OVP/OCP Delay
  PAR_FAN,  //Fan Start Temperature
  PAR_ALA,  //Alarm Temperature
  PAR_HST,  //Heatsink Measured Temperature
  PAR_INF,  //Firmware Version Info
  PAR_DEF,  //Set Defaults
  PAR_CAL,  //Calibration (NO/YES/DEFAULT)
  PAR_ESC,  //����� �� ���� (NO/YES)
  PARS_SETUP  
};

enum ParType_t //��� ���������
{
  PT_V,     //����������, x0.01 V
  PT_I,     //���, x0.001 A
  PT_P,     //�������� x0.1 W
  PT_VC,    //��� ����������
  PT_IC,    //��� ����
  PT_PRE,   //CALL/STORE (NOSAVE)
  PT_OFFON, //OFF/ON
  PT_FALN,  //OFF/ALARM/ON
  PT_APHPL, //AVERAGE/PEAK HIGH/PEAK LOW
  PT_DEL,   //��������, ��
  PT_T,     //�����������, x0.1�C
  PT_FIRM,  //Firmware Version (NOSAVE)
  PT_NY,    //����� ������ (NOSAVE)
  PT_NYDEF  //NO/YES/DEFAULT (NOSAVE)
};

enum OffOn_t { OFF, ON };
enum NoYes_t { NO, YES, DEFAULT };

#define ON_FLAG 0x8000 //���� ��������� ������

#define VER ((uint16_t)(VERSION * 100))

//----------------------------------------------------------------------------
//----------------------------- ����� TParam: --------------------------------
//----------------------------------------------------------------------------

class TParam
{
private:
  char Name[DIGS + 1];
public:
  char Type;
  TParam(ParType_t type, const char *s,
         uint16_t min, uint16_t nom, uint16_t max);
  uint16_t Min;
  uint16_t Nom;
  uint16_t Max;
  uint16_t Value;
  void ShowName(void);
  void ShowValue(void);
  bool Savable(void);
  bool Validate(void);
  bool Edit(int16_t step);
};

//----------------------------------------------------------------------------
//-------------------------- ��������� ����� TList: --------------------------
//----------------------------------------------------------------------------

template<class T>
class TList
{
private:
  char ItemsMax;
public:
  TList(char max);
  T** Items;
  char ItemsCount;
  void AddItem(T *t);
};

//-------------------------- ���������� �������: -----------------------------

template<class T>
TList<T>::TList(char max)
{
  ItemsMax = max;
  Items = new T*[ItemsMax];
  ItemsCount = 0;
}

template<class T>
void TList<T>::AddItem(T *t)
{
  if(ItemsCount < ItemsMax)
    Items[ItemsCount++] = t; 
}

//----------------------------------------------------------------------------
//--------------------------- ����� TParamList: ------------------------------
//----------------------------------------------------------------------------

class TParamList : public TList<TParam>
{
private:
public:
  TParamList(char max) : TList(max) {};
  TEeSection *EeSection;
  void LoadDefaults(void);
  void ReadFromEeprom(char n);
  void ReadFromEeprom(void);
  void SaveToEeprom(char n);
  void SaveToEeprom(void);
};

//----------------------------------------------------------------------------
//----------------------------- ����� TData: ---------------------------------
//----------------------------------------------------------------------------

class TData
{
public:
  TData(void);
  TParamList *TopData;
  TParamList *MainData;
  TParamList *ProtData;
  TParamList *SetupData;
  TEeSection *PresetV;
  TEeSection *PresetI;
  TRingSection *Ring;
  bool OutOn;
  void SetVI(void);
  void Apply(char par);
  void ApplyAll(void);
  void TrimParamsLimits(void);
  void ReadV(void);
  void SaveV(void);
  void InitPresets(void);
  void ReadPreset(char n);
  void SavePreset(char n);
};

//----------------------------------------------------------------------------

extern TData *Data;

//----------------------------------------------------------------------------

#endif
