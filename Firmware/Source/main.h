//----------------------------------------------------------------------------

//������� ������, ������������ ����

//----------------------- ������������ �������: ------------------------------

//PA0 - ������ EF1 ��������
//PA1 - ������ EF2 ��������
//PA2 (USART2 TX) - ������ OWPO ���� OWP ����������
//PA3 (USART2 RD) - ������ OWPI ���� OWP ����������
//PA4 (DAC1 + DMA CH2 + TIM3) - ���������� ����� SET_I
//PA5 (DAC2 + DMA CH3 + TIM3) - ���������� ����� SET_V
//PA6 (ADC6 + DMA CH5 + TIM2) - ���������� ���� GET_I
//PA7 (ADC7 + DMA CH7 + TIM2) - ���������� ���� GET_V
//PB0 - ���� ������ ��������� CC/CV
//PB1 - ����� ��������� ��������� ON
//PB10 - ������ ��������� ������ BT_OUT
//PB11 - ������ ������ ����������� BT_FINE
//PB12 - ������ ��������� ���� BT_SET_I
//PB13 (TIM1 CH1N) - ����� ����� SND
//PB14 (TIM15 CH1) - ����� ��� ����������� FAN
//PB15 - ������ ��������� ���������� BT_SET_V
//PA8 - ������ �������� SB
//PA9 (USART1 TXD) - ����� ����� TXD
//PA10 (USART1 RXD) - ���� ����� RXD
//PB3 (SPI1 SCK) - ����� SCLK ���� SPI �������� �������
//PB5 (SPI1 MOSI) - ����� SDATA ���� SPI �������� �������
//PB6 - ����� LOAD �������� �������
//PB7 - ����� OE �������� �������
//PB8 (+ TIM16) - ������ SCL ���� I2C ������� EEPROM 24C04
//PB9 (+ TIM16) - ������ SDA ���� I2C ������� EEPROM 24C04

//������������ ����� ������� Linker->Extra options: --basic_heap

//----------------------------------------------------------------------------

#ifndef MAIN_H
#define MAIN_H

//----------------------------------------------------------------------------

#include <stdint.h>
#include "stm32f10x.h"
#include "gpio.h"
#include "timer.h"

//------------------------- ����� ������ firmware: ---------------------------

#define VERSION 1.00

//--------------------------- ����������������: ------------------------------

#define LO(x)    ((uint8_t)((x) & 0xFF))
#define HI(x)    ((uint8_t)(((x) >> 8) & 0xFF))
#define BYTE1(x) (LO(x))
#define BYTE2(x) (HI(x))
#define BYTE3(x) ((uint8_t)(((uint32_t)(x) >> 16) & 0xFF))
#define BYTE4(x) ((uint8_t)(((uint32_t)(x) >> 24) & 0xFF))

#define WORD(b1,b0)        (((uint16_t)(b1) << 8) | (b0))
#define DWORD(b3,b2,b1,b0) (((uint32_t)WORD(b3,b2) << 16) | WORD(b1,b0))

#define ABS(x) ((x < 0)? -x : x)  

#ifndef NULL
  #define NULL _NULL
#endif

//----------------------------------------------------------------------------

#endif
