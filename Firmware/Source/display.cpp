//----------------------------------------------------------------------------

//������ ��������� LED-�������

//----------------------- ������������ �������: ------------------------------

//������������ ������� �� 7-���������� LED-�����������.
//����������� ������� - 2 ������ �� 4 �������.
//��������� - ������������, ����������� ������������������� 4.
//�������� � ����-����� ���������� ������� �������.
//������������� ������������ 4 ����������:
//LED_CC, LED_CV, LED_OUT � LED_FINE.
//���������� ���������� ������� �������.
//���������� � ���������� ���������� ����� ������� ���������
//�������, ������ � �������� �������������� ����� ����� TSreg.

//----------------------------------------------------------------------------

#include "main.h"
#include "display.h"

//----------------------------- ���������: -----------------------------------

#define BLINK_TM 400 //������ �������, ��
#define BLDEL_TM 800 //�������� ������ �������, ��

//������������ ����� ��������� ����������:

#define _F_ 0x01
#define _A_ 0x02
#define _B_ 0x04
#define _G_ 0x08
#define _C_ 0x10
#define _H_ 0x20
#define _E_ 0x40
#define _D_ 0x80

//������������ ����� ����-����� � �����������:

#define LED_OUT  0x01 //��������� "OUT ON/OFF"
#define SCAN1    0x02 //����� ������������ 1
#define SCAN2    0x04 //����� ������������ 2
#define SCAN3    0x08 //����� ������������ 3
#define SCAN4    0x10 //����� ������������ 4
#define LED_FINE 0x20 //��������� "FINE"
#define LED_CC   0x40 //��������� "CC"
#define LED_CV   0x80 //��������� "CV"

//----------------------------------------------------------------------------
//---------------------------- ����� TDisplay --------------------------------
//----------------------------------------------------------------------------

//----------------------------- �����������: ---------------------------------

TDisplay::TDisplay(void)
{
  Sreg.Init();
  Sreg = 0;
  Sreg.Enable();
  BlinkTimer = new TSoftTimer();
  LedCV = 0;
  LedCC = 0;
  LedOut = 0;
  LedFine = 0;
  BlinkEn = BLINK_NO;
  BlinkOn = 1;
  DispOn = 1;
  Clear();
}

//------------ �������������� ���� ������� � 7-���������� ���: ---------------

//��� ���� ������������ ���� 0..F. ��� ���� � ����������� ��������
//������������ ����, ����������� � ���������� Win-1251.
//������������ ��������� �� ����������:
//
//    -- A --
//   |       |
//   F       B
//   |       |
//    -- G --
//   |       |
//   E       C
//   |       |
//    -- D --  (H)

char TDisplay::Conv(char d)
{
  static const char Font[]= //������� ���������������
  {
    _A_+_B_+_C_+_D_+_E_+_F_    , //��� 00H, ������ "0"
        _B_+_C_                , //��� 01H, ������ "1"
    _A_+_B_+    _D_+_E_+    _G_, //��� 02H, ������ "2"
    _A_+_B_+_C_+_D_+        _G_, //��� 03H, ������ "3"
        _B_+_C_+        _F_+_G_, //��� 04H, ������ "4"
    _A_+    _C_+_D_+    _F_+_G_, //��� 05H, ������ "5"
    _A_+    _C_+_D_+_E_+_F_+_G_, //��� 06H, ������ "6"
    _A_+_B_+_C_                , //��� 07H, ������ "7"
    _A_+_B_+_C_+_D_+_E_+_F_+_G_, //��� 08H, ������ "8"
    _A_+_B_+_C_+_D_+    _F_+_G_, //��� 09H, ������ "9"
    _A_+_B_+_C_+    _E_+_F_+_G_, //��� 0AH, ������ "A"
            _C_+_D_+_E_+_F_+_G_, //��� 0BH, ������ "b"
                _D_+_E_+    _G_, //��� 0CH, ������ "c"
        _B_+_C_+_D_+_E_+    _G_, //��� 0DH, ������ "d"
    _A_+        _D_+_E_+_F_+_G_, //��� 0EH, ������ "E"
    _A_+            _E_+_F_+_G_, //��� 0FH, ������ "F"
    _A_+    _C_+_D_+_E_+_F_    , //��� 10H, ������ "G"
        _B_+_C_+    _E_+_F_+_G_, //��� 11H, ������ "H"
                _D_+_E_+_F_    , //��� 12H, ������ "L"
            _C_+    _E_+    _G_, //��� 13H, ������ "n"
            _C_+_D_+_E_+    _G_, //��� 14H, ������ "o"
    _A_+_B_+        _E_+_F_+_G_, //��� 15H, ������ "P"
                    _E_+    _G_, //��� 16H, ������ "r"
                _D_+_E_+_F_+_G_, //��� 17H, ������ "t"
            _C_+_D_+_E_        , //��� 18H, ������ "u"
        _B_+_C_+_D_+_E_+_F_    , //��� 19H, ������ "U"
        _B_+_C_+_D_+    _F_+_G_, //��� 1AH, ������ "Y"
    _A_+        _D_+_E_+_F_    , //��� 1BH, ������ "�"
    _A_+_B_+            _F_+_G_, //��� 1CH, ������ "�"
                            _G_, //��� 1DH, ������ "-"
                             0   //��� 1EH, ������ " "
  };
  if(d >= 0x30 && d <= 0x39) d -= 0x30;
  if(d > 0x0F)
  {
    switch(d)
    {
    case 'A': d = 0x0A; break; //������ "A"
    case 'b': d = 0x0B; break; //������ "b"
    case 'c': d = 0x0C; break; //������ "c"
    case 'C': d = 0x1B; break; //������ "C"
    case 'd': d = 0x0D; break; //������ "d"
    case 'E': d = 0x0E; break; //������ "E"
    case 'F': d = 0x0F; break; //������ "F"
    case 'G': d = 0x10; break; //������ "G"
    case 'H': d = 0x11; break; //������ "H"
    case 'I': d = 0x01; break; //������ "I"
    case 'L': d = 0x12; break; //������ "L"
    case 'n': d = 0x13; break; //������ "n"
    case 'o': d = 0x14; break; //������ "o"
    case 'O': d = 0x00; break; //������ "O"
    case 'P': d = 0x15; break; //������ "P"
    case 'r': d = 0x16; break; //������ "r"
    case 'S': d = 0x05; break; //������ "S"
    case 't': d = 0x17; break; //������ "t"
    case 'u': d = 0x18; break; //������ "u"
    case 'U': d = 0x19; break; //������ "U"
    case 'Y': d = 0x1A; break; //������ "Y"
    case '*': d = 0x1C; break; //������ "�"
    case ' ': d = 0x1E; break; //������ " "
    default:  d = 0x1D;        //������ "-"
    }
  }
  return(Font[d]); //���������� 7-���������� ���
}

//------------------------ ������������ �������: -----------------------------

void TDisplay::Execute(void)
{
  if(TSysTimer::Tick)
  {
    static char Phase = POS_1;
    char Scans = SetScan(Phase);
    char DataV = SegDataV[Phase];
    char DataI = SegDataI[Phase];

    if(BlinkEn)
    {
      if(BlinkTimer->Over())
      {
        BlinkTimer->Start(BLINK_TM / 2);
        BlinkOn = !BlinkOn;
      }
      if(!BlinkOn)
      {
        if(BlinkEn & BLINK_V) DataV = 0;
        if(BlinkEn & BLINK_I) DataI = 0;
      }
    }

    Sreg = DWORD(0, Scans, DataV, DataI);
    if(++Phase == DIGS) Phase = POS_1;
  }
}

//------------ ��������� ��������� ����-����� � �����������: -----------------

inline char TDisplay::SetScan(char phase)
{
  char c = 0;
  if(DispOn)
  {
    if(phase == POS_1) c |= SCAN1;
    if(phase == POS_2) c |= SCAN2;
    if(phase == POS_3) c |= SCAN3;
    if(phase == POS_4) c |= SCAN4;
  }
  if(LedCV)   c |= LED_CV;
  if(LedCC)   c |= LED_CC;
  if(LedOut)  c |= LED_OUT;
  if(LedFine) c |= LED_FINE;
  return(c);
}

//--------------------- ���������/���������� �������: ------------------------

void TDisplay::Blink(Blink_t blink)
{
  BlinkEn = blink;
  BlinkTimer->Start(BLINK_TM / 2);
  BlinkOn = 0; //������� ���������� � �������
}
    
//--------------------------- ������� �������: -------------------------------

void TDisplay::Clear(void)
{
  for(char i = 0; i < DIGS; i++)
  {
    SegDataV[i] = 0;
    SegDataI[i] = 0;
  }
  Row = ROW_V; Pos = POS_1;
}

//-------------------------- ���������� �������: -----------------------------

void TDisplay::Off(void)
{
  DispOn = 0;
  char Scans = SetScan(0);
  Sreg = DWORD(0, Scans, 0, 0);
}

//--------------------------- ��������� �������: -----------------------------

void TDisplay::On(void)
{
  DispOn = 1;
}

//----------------- ���������� ������� ������ �� ������������: ---------------

void TDisplay::Disable(void)
{
  Sreg.Disable();
}

//----------------- ��������� ������� ������ �� ������������: ----------------

void TDisplay::Enable(void)
{
  Sreg.Enable();
}

//-------------------------- ��������� �������: ------------------------------

void TDisplay::SetPos(char row, char pos)
{
  if(row < ROWS && pos < DIGS)
  {
    Row = row; Pos = pos;
  }
}

//--------------------------- ����� �������: ---------------------------------

void TDisplay::PutChar(char ch)
{
  if(((BlinkEn & BLINK_V) && (Row == ROW_V)) || //������������ ������� V
     ((BlinkEn & BLINK_I) && (Row == ROW_I)))   //������������ ������� I
  {
    BlinkOn = 1;
    BlinkTimer->Start(BLDEL_TM);
  }
  char s = ((ch & POINT)? _H_ : 0) + Conv(ch & ~POINT);
  if(Row == ROW_V) SegDataV[Pos] = s;
  if(Row == ROW_I) SegDataI[Pos] = s;
  if(++Pos == DIGS)
  {
    Pos = POS_1;
    if(++Row == ROWS)
      Row = ROW_V;
  }
}

//------------------ ����� null-terminated string �� RAM: --------------------

void TDisplay::PutString(char *s)
{
  while(*s) PutChar(*s++);
}

//------------------ ����� null-terminated string �� ROM: --------------------

void TDisplay::PutString(const char *s)
{
  while(*s) PutChar(*s++);
}

//---------- ��������������� ����� ������ ����� � �������� ������: -----------

//n - ����� ���������� ��������� ���� (� ������
//������� "�����" ��� ������������� ��������),
//d - ���������� ���� ����� �����.
//������ ��������� n ��������, ��������� ������� �� ����������.

//�������:
//v =    1,  n = 4, d = 0:     1
//v =    1,  n = 4, d = 3: 0.001
//v = 9999,  n = 4, d = 3: 9.999
//v = 10000, n = 4, d = 3: -.---
//v =    -1, n = 4, d = 3: -.---
//v =    -1, n = 4, d = 0:    -1
//v =  -999, n = 4, d = 2: -9.99
//v =   999, n = 3, d = 1: 99.9

//������ ������������������� (n = 4, d = 3 + AUTO_SCALE):
//v =        9: 0.009
//v =     9999: 9.999
//v =    99999: 99.99
//v =   999999: 999.9
//v =  9999999:  9999
//v = 10000000:  ----
//v =       -9: -0.00
//v =      -99: -0.09
//v =    99999: -99.9
//v =   999999:  -999
//v =  1000000:  ----

#define MAX_DIGITS 10 //����� ������ ��� 32-������� �����

void TDisplay::PutIntF(int32_t v, char n, char d)
{
  char buff[MAX_DIGITS];
  char i;
  //�������� ����� �����:
  bool minus = v < 0;
  if(minus) v = -v;
  //�������������� ����� � ������:
  for(i = 0; i < MAX_DIGITS; i++)
  {
    char dig = v % 10;
    v /= 10;
    buff[i] = dig;
  }
  //���������� �����:
  char pnt = d & ~AUTO_SCALE;
  if(pnt >= MAX_DIGITS) pnt = 0;
  buff[pnt] |= POINT;
  //�������� ������������:
  char j = n - (minus? 1 : 0);
  if(d & AUTO_SCALE) j += d & ~AUTO_SCALE;
  bool overflow = 0;
  for(i = j; i < MAX_DIGITS; i++)
    overflow |= buff[i];
  if(overflow)
  {
    for(i = 0; i < MAX_DIGITS; i++)
      buff[i] = (i < n)? '-' : 0;
    if(!(d & AUTO_SCALE))
      buff[pnt] |= POINT;
    minus = 0;  
  }
  //���������� ���������� �����:
  i = MAX_DIGITS;
  while(!buff[--i])
    buff[i] = ' ';
  //���������� ������:
  if(minus) buff[++i] = '-';
  //���������� ������� ������� ������� ��� ������:
  if(i < n - 1) i = n - 1;
  //�������� ����� � ������� ������ �������:
  buff[i - (n - 1)] &= ~POINT;
  //����� �����:
  for(char j = 0; j < n; j++)
    PutChar(buff[i--]);
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
